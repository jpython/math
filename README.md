# Mathématiques

18/02/2021 - v1.0c

mise à jour mineure : ven. 16 juin 2023 17:47:52 CEST (add figure)

## Support de cours : Mathématiques pour l'informatique

Contenu PDF à jour : https://framagit.org/jpython/math/-/raw/master/mathinfo1/Docs/Teach/mathinfo1.pdf?inline=false

## Plan du cours
- Théorie des ensembles
- Combinatoire
- Probabilités
- Logique et calculabilité
- Algèbre linéaire
- Complexité algorithmique
- Théorie des graphes
- Références

## Article (vulgarisation) : Diagonalisation de l'hôtel de Hilbert

PDF : https://framagit.org/jpython/math/-/raw/master/Hotel_Hilbert/hotel_hilbert.pdf?inline=false

#!/usr/bin/env python3

#from itertools import count as N

e1 = { 1, 3, 4, 7, 8 }

def indic(s):
    def _(x):
        return 1 if x in s else 0
    return _

def indic2(s):
    return (lambda x: 1 if x in s else 0)

Ie1 = indic(e1)
for i in range(20):
    print(f"{i} dans e1 ? ",Ie1(i))


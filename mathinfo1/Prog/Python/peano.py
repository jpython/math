#!/usr/bin/env python3

def S(x):
    return x.S()

class Peano(str):
    def __new__(cls, ch='0', debug=False):
        me = super().__new__(cls, ch)
        me.debug = debug
        return me
    def log(self, *args, **kwargs):
        print(*args, **kwargs)
    def S(self):
        return Peano('S' + self)
    def __int__(self):
        return self.count('S')
    #def __str__(self):
    #    return super().__repr__() + f" ({int(self)})"
    def __add__(self, other):
        if other == '0':
            self.log('rule a0 add', f"{self} + {other} -> {self}")
            return self
        else:
            self.log('rule a1 add', f"{self} + {other} -> S({self} + {other[1:]})")
            return S(self + other[1:])
    def __mul__(self, other):
        if other == '0':
            self.log('rule m0 mul', f"{self} x {other} -> 0")
            return Peano() # Zéro
        else:
            self.('rule m1 mul', f"({self} x {other[1:]}) + {self}")
            return (self * other[1:]) + self

if __name__ == '__main__':
    zero = Peano()
    un   = zero.S()
    deux = zero.S().S()
    trois = deux.S()
    quatre = S(trois)
    print('  Zéro', zero)
    print('    Un', un)
    print('  Deux', deux)
    print(' Trois', trois)
    print('Quatre', quatre)
    print(' deux + deux  = ', deux + deux, f"({int(deux + deux)})")
    print(' deux * trois = ', deux * trois, f"({int(deux * trois)})")


#!/usr/bin/env python3

from peano_abc import Peano_operators, P, S

class Peano(str,Peano_operators):
    def __new__(cls, ch='0', prev=None):
        self = super().__new__(cls, ch)
        self.prev = prev
        return self
    def S(self):
        return Peano('S' + self)
    def is_zero(self):
        return self == '0'
    @classmethod
    def Zero(cls):
        return cls.__new__('0')
    def __int__(self):
        return self.count('S')
    #def __str__(self):
    #    return super().__repr__() + f" ({int(self)})"

if __name__ == '__main__':
    zero = Peano()
    un   = zero.S()
    deux = zero.S().S()
    trois = deux.S()
    quatre = S(trois)
    print('  Zéro', zero)
    print('    Un', un)
    print('  Deux', deux)
    print(' Trois', trois)
    print('Quatre', quatre)
    print(' deux + deux  = ', deux + deux, f"({int(deux + deux)})")
    print(' deux * trois = ', deux * trois, f"({int(deux * trois)})")


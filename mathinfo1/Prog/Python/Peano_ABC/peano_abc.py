#!/usr/bin/env python3

from abc import ABC, abstractmethod

def S(x):
    return x.S()

def P(x):
    return x.P()

class Peano_operators(ABC):
    @abstractmethod
    def S(self):
        pass
    def P(self):
        return self.prev 
    @abstractmethod
    def is_zero(self):
        pass
    @abstractmethod
    def Zero(cls):
        pass
    def __add__(self, other):
        if other.is_zero() == '0':
            print('rule a0 add', f"{self} + {other} -> {self}")
            return self
        else:
            print('rule a1 add', f"{self} + {other} -> S({self} + P({other})")
            return S(self + other.P())
    def __mul__(self, other):
        if other.is_zero():
            print('rule m0 mul', f"{self} x {other} -> 0")
            return self.Zero() 
        else:
            print('rule m1 mul', f"({self} x P({other}) + {self}")
            return (self * other.P()) + self

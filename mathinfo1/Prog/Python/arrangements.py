#!/usr/bin/env python3

from itertools import chain, permutations, combinations

def k_permutations(it, k):
    return chain( *( (permutations(c)) for c in combinations(it, k) ) )

if __name__ == '__main__':
    foods = 'ham spam egg bacon'.split()
    for arr in k_permutations(foods, 3):
        print(*arr)


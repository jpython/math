#!/usr/bin/env python3

# TODO: implement +/* rules as abstract base class (mixin)
# only needed is syntactic "P" (predecessor)
# to be done for Peano and Zermelo too...

def S(x):
    return x.S()

class VonNeumann(frozenset):
    def __new__(cls, val=()):
        return super().__new__(cls, val)
    def S(self):
        return VonNeumann( self | frozenset( [ self ] ))
    def __repr__(self):
        if self == frozenset():
            return '∅'
        else:
            return '{ ' + \
             ', '.join( sorted([ str(elt) for elt in self], key=len) ) + \
                   ' }' 
    def __int__(self):
        return len(self)
    def __add__(self, other):
        pass
    def __mul__(self, other):
        pass

if __name__ == '__main__':
    zero = VonNeumann()
    un   = zero.S()
    deux = zero.S().S()
    trois = deux.S()
    quatre = S(trois)
    print('  Zéro', zero)
    print('    Un', un)
    print('  Deux', deux)
    print(' Trois', trois)
    print('Quatre', quatre, int(quatre))


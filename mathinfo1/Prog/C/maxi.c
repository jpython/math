#include<stdio.h>
#include<stdlib.h>

int main() {
  int n1     = 2147483647;          /* 2**32 - 1 */
  int64_t n2 = 9223372036854775807; /* 2**64 - 1 */
  printf("%+d\t%+lld\n", n1, n2);
  printf("%+d\t%+lld\n", n1 + 1, n2 + 1);
  exit(0);
}

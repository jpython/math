% Mathématiques pour l'informatique
% Jean-Pierre Messager (jp@xiasma.fr)
% 21 janvier 2021 -- version 1.0b

# Utilisation commerciale interdite sans autorisation
\begin{minipage}{\linewidth}
\input{Media/cc-by-nc-nd.tex}
\end{minipage}

\NoAutoSpacing

# Télécharger ce cours à jour et y contribuer

## Il est disponible sur un dépot git
- Accès public à ce cours : \url{https://framagit.org/jpython/math}
- Si vous créez un compte sur \url{https://framagit.org/}
- Transmettez-moi votre identifiant
- Je vous accorde le status _reporter_ sur ce projet
  - Ouverture de tickets
- D'autres projets : \url{https://framagit.org/jpython/meta}
- Des mises à jours sont régulièrement disponibles
- Chaque changement important de version est accompagné d'un _tag_

# Mathématiques pour l'informatique 

## Plan du cours
- Théorie des ensembles
- Combinatoire
- Probabilités
- Logique et calculabilité
- Algèbre linéaire
- Complexité algorithmique
- Théorie des graphes
- Références

# Mathématiques pour l'informatique 

_« Matematyczny ład świata to nasza modlitwa do piramidy chaosu. »_

_« L'ordre mathématique du monde est notre prière à la pyramide du chaos. »_

\begin{flushright}
Stanislas \textsc{Lem} (1921--2006)
\end{flushright}

\begin{center}
~\\\vspace{0.5cm}
\includegraphics[width=.3\textwidth]{Media/stanislasLemDog1.jpg}
\end{center}

#

\begin{flushright}
pour Kate et Kelly
\end{flushright}

# Présentation

Ce que nous allons évoquer ici est le résultat de plusieurs millénaires
de pratiques et de réflexions mathématiques, scientifiques, esthétiques et
philosophiques. En particulier à l'articulation du XIXe et du XXe siècle
la _crise des fondements_ a amené à formuler l'ensemble des mathématiques
en termes d'ensembles et à mettre à plat la logique formelle.

Ces travaux sont intrinsinquement liés aux réflexions sur la _calculabilité_
qui allaient amener à la naissance de l'informatique moderne : matériels comme
logiciels.

D'autres champs des mathématiques sont évoqués du fait de leur importance
en programmation : probabilités, algèbre linéaire.

Beaucoup de propositions sont affirmées _sans démonstration_. Elles
existent bien entendu. De même des termes sont introduits sans définitions
(_corps_, _anneau_, ...) ; elles se trouvent aisément sur Wikipedia.

#

Des pans entiers sont, hélas, négligés car ils nécessiteraient, chacun,
un cours entier : géométries, calcul différentiel et intégral, analyse
numérique, topologie, calcul formel, arithmétique, cryptographie, théorie
des codes, théorie des jeux, statistiques, etc. 

La présentation s'efforce d'éviter tout excès de formalisme,
la théorie axiomatique _ZFC_, par exemple, n'est pas présentée. Le lecteur
pourra s'y référer s'il cherche un peu plus de _rigueur_.

L'histoire ne s'est pas arrêtée là. La logique intuitionniste a donné
naissance à toute une branche des mathématiques et à une façon de programmer
qui permet de _prouver_ qu'un programme n'a pas de _bugs_ (système _CoQ_ 
de l'INRIA par exemple).

Nous n'avons, ici, guère plus d'ambition que de nous prendre au jeu et
d'éveiller notre curiosité. Chaque domaine n'est, au fond, qu'à peine
_effleuré_. Il reste bien d'autres fruits à découvrir.

# Note typographique

Ce document est rédigé en _Markdown_, format texte assez simple complété
par le système de composition \LaTeX, basé sur \TeX{} de Donald \textsc{Knuth},
(particulièrement adapté à la composition de textes mathématiques mais pas
seulement)
ce qui permet à _Beamer_ \LaTeX{} (via _pandoc_) de construire le document final. Je ne saurais trop
vous en recommander l'usage, en particulier \LaTeX.

$$
\underbrace{\ln \left( \frac{\pi}{4} \right)}_{\simeq -0,241564}
< \overbrace{\sin \left(\frac{4}{\pi} \right)}^{\simeq 0.9560555}
$$

\tiny Image de couverture : Maurits Cornelis \textsc{Escher} -- _Möbius Strip II_ (1963)

# Qu'est-ce qu'une démonstration mathématique ?

##
Une démonstration est une successions d'énoncés qui, partant
d'hypothèses et de vérités supposées (axiomes) établit une
conclusion supposée valide : propriété d'un objet abstrait,
existence d'un certain objet, inexistence ou impossibilité d'un
autre.

##
Une démonstration vise d'abord à _convaincre_ de la validité d'un
théorème ; se convaincre déjà soi-même d'une intuition, ou l'invalider,
puis de convaincre un _public_ donné, invité à approuver ou infirmer
la validité d'une _preuve_ espérée. 

##
La nature des arguments utilisé, leur niveau de détail, dépendent du public
visé. Dans l'enseignement une _démonstration_ est demandée aussi pour
une autre raison : _convaincre_ le lecteur que l'on a bien assimilé une
technique de preuve ou un cadre conceptuel.

# Démonstrations et automates

\begin{small}
\emph{Alors, il ne sera plus besoin entre deux philosophes de discussions plus longues qu'entre deux mathématiciens, puisqu'il suffira qu'ils saisissent leur plume, qu'ils s'asseyent à leur table de calcul (en faisant appel, s'ils le souhaitent, à un ami) et qu'ils se disent l'un à l'autre : « Calculons ! »}

\begin{flushright} 
--- Gottfried Wilhelm \textsc{Leibniz},\\ \emph{Nova methodus pro maximis et minimis \emph{in} Acta Eruditorum}, 1684
\end{flushright} 
\end{small}

##
Si les _règles_ de déduction utilisées pour passer d'une proposition à
une autre dans une preuve sont suffisemment précises et mécaniques
on peut envisager d'en _automatiser_ la rédaction et la validation
(_« pensée aveugle »_ de Leibniz) voire de l'implémenter dans
une _machine_.

##
En ce sens une preuve est une sorte de _programme_ informatique. Cette
intuition est formalisée rigoureusement par l'_isomorphisme de
Curry-Howard_.

# Exemple : $\sqrt{2}$ est irrationnel

##
Supposons qu'il existe $p$, $q$, entiers premiers entre eux (pas
de facteurs communs) tels que :
$$ \sqrt{2} = \frac{p}{q} $$
$$ \Rightarrow p^2 = 2 q^2 $$ 
donc $p^2$ est pair, et donc $p$ aussi : $p = 2k$, mais alors :
$$ p^2 = (2k)^2 = 4 k^2 = 2 q^2$$
$$ \Rightarrow q^2 = 2 k^2 $$
Donc $q^2$ est pair, donc $q$ aussi, tout comme $p$.

## 
Mais, par hypothèse, $q$ et $p$ n'ont pas de facteur commun... L'hypothèse
de départ est donc _fausse_ : $\sqrt{2}$ est _irrationnel_. 

# Exemple : infinité des nombres premiers

##
Soient, pour $n \in \mathbb{N}^{\ast}$, $p_1 =2,\; p_2 =3,\; p_3 = 5,\;
p_4 = 7,\; \ldots,\; p_n$
les $n$ premiers nombres premiers consécutifs. Considérons le nombre :

$$ N = p_1\times p_2\times p_3\times p_4\times \ldots\times p_n + 1 $$

Pour tout $n$ le reste de la division euclidienne de $N$ par $p_n$
est égal à $1$.

$N$ n'a donc aucun facteur premier dans la liste
$(p_i)_{1\leqslant i \leqslant n}$.

Il a donc un facteur premier _plus grand_ (il n'y a pas de
nombre premiers plus petits que $p_n$).

##
Pour toute liste des $n$ premiers nombres premiers, on peut en trouver un
_autre_, plus _grand_. Ils sont donc en quantité infinie, inépuisables.

# Exemple : existence non-constructive

##
Question : existe-t-il $a$ et $b$ irrationnels tels que $a^b$ soit rationnel ?

##
Considérons la proposition $(*)$ : $\displaystyle\sqrt{2}^{\sqrt{2}}$ est irrationnel.

De deux choses _l'une_ : ou bien elle est _vraie_, ou bien elle est _fausse_.

##
Si elle est _vraie_, alors la réponse à la question est _oui_ : il suffit de 
prendre $a = b = \sqrt{2}$.

Si elle est _fausse_, alors on peut prendre $a = \sqrt{2}^{\sqrt{2}}$ et
$b = \sqrt{2}$.

$a$ est irrationnel (puisque $(*)$ est _fausse_) et
$b$ aussi, mais alors :
$$ a^b = \left(\sqrt{2}^{\sqrt{2}}\right)^{\sqrt{2}} = \sqrt{2}^{\sqrt{2}\times\sqrt{2}} = \sqrt{2}^2 = 2 $$
$2$ étant rationnel, la réponse à la question est aussi _oui_.

##
Donc la réponse est _oui_. Euh ?

# Paradoxes

## Il arrive que notre intuition soit prise en défaut...
Il existe des courbes du plan qui sont _continues_ et n'ont de _tangentes_
quasiment _null part_ _i.e._ des _fonctions_ de $\mathbb{R}$ dans $\mathbb{R}$
partout _continues_ et _presque partout_ (sauf en des points isolés)
non-dérivables.

## 
On peut découper une sphère en un nombre _fini_ de morceaux et réarranger
les morceaux pour fabriquer _deux_ sphères de même taille (paradoxe de
Banach-Tarski).

##
Il existe des parties de la droite réelle qui n'ont pas de _longueur_
(même pas $0$ ou $+\infty$) mais on ne peut en définir aucune.

##
Les ensembles _infinis_ sont, d'une certaine façon, ausi _grands_
que certaines de leurs _parties strictes_. 

# Paradoxe... ou contradiction ?

##
Si l'_ensemble de tous les ensembles_ existe, on peut consider une de
ses parties : l'ensemble $U$ des ensembles qui _n'appartiennent pas
à eux-mêmes_.

## Est-ce que $U\in U$ ?
Si _oui_, alors $U\notin U$ (par définition de $U$) et donc _non_.

Si _non_, alors $U\in U$ (par définition de $U$) et donc _oui_.

##
Il _n_'existe donc _pas_ d'ensemble de tous les ensembles !

## La barbe !
Dans un village peut-on dire que le barbier ne rase que les gens qui
ne se rasent pas eux-mêmes ?


# Pourquoi la théorie des ensembles ?

## 
- Les mathématiques comprennent la géométrie, l'arithmétique, l'algèbre,
  l'analyse, etc.
- À la fin du XIXe siècle la discipline apparaît très fragmentée même si
  les méthodes de démonstration sont communes
- Des paradoxes sont signalés ici et là...
- Les intuitions premières sont parfois fausses

## 
- Recherche d'un ensemble de notions _simples_ unificatrices pour encoder
  tout ça
- Georg \textsc{Cantor}, lors de recherches en analyse (théorie de
  Fourier), propose les ensembles
- D'autres propositions arriveront au XXe siècle : _fonctions_ (lambda-calcul
  de Church),
  _symboles_ (logique combinatoire de Schönfinkel),
  formalisme logique (Russell, Whitehead, Hilbert), _flèches_
  (théorie des catégories)

# Ensembles

## Définition informelle
Un ensemble est une collection d'objets distincts

On note $\in$ la relation _« est un élément de »_ 

Un ensemble est totalement défini par ses éléments :

Si $x \in E \Rightarrow x \in F$ **et** si $x \in F \Rightarrow x \in E$,
alors $E = F$, en d'autres termes :

$$E = F \iff ( x \in E \iff x \in F )$$

## Inclusion entre ensembles

Un ensemble $E$ est inclus dans un ensemble $F$ ssi tous les éléments de E
sont éléments de F.
$$E \subset F \iff ( x \in E \Rightarrow x \in F )$$

# Ensemble vide, union, intersection

## L'ensemble vide

L'ensemble vide, noté $\{\}$ ou $\varnothing$ est l'unique ensemble qui n'a
aucun élément : $x\in \varnothing$ est toujours _faux_.


## Union
L'union de deux ensemble est l'ensemble des éléments qui appartiennent
à l'un **ou** à l'autre.

$$ x \in E \cup F \iff x\in E \mbox{ ou } x\in F $$

## Intersection
L'intersection de deux ensembles est l'ensemble des éléments qui appartiennent
à l'un **et** à l'autre.

$$ x\in E \cap F \iff x\in E \mbox{ et } x\in F $$

# Exercices

##
Prouver qu'il n'existe qu'un seul ensemble vide.

## 
Prouver que si $E \subset F$ alors $E \cup F = F$ et $E \cap F = E$.

## 
Prouver que si $E \cup F = F$ alors $E \subset F$ et que si $E \cap F = E$
alors $E \subset F$.

##
Prouver que : $E \cup (F \cap G) = (E \cup F) \cap (E \cup G)$

et que $E \cap (F \cup G) = (E \cap F) \cup (E \cap G)$

## 
Soit $U$ tel que $E\subset U$ et $F\subset U$. On note :

$\overline{E} = U \backslash E = \{ x \in U : x \notin E \}$.

Prouver que $\overline{E\cup F} = \overline{E}\cap \overline{F}$
et $\overline{E\cap F} = \overline{E}\cup \overline{F}$



# Diagrammes de Venn

## Intersection et union
\def\firstcircle{(0,0) circle (1.1cm)}
\def\secondcircle{(0:1.4cm) circle (1.1cm)}

\colorlet{circle edge}{blue!50}
\colorlet{circle area}{blue!20}

\tikzset{filled/.style={fill=circle area, draw=circle edge, thick},
    outline/.style={draw=circle edge, thick}}

\begin{center}
\begin{tabular}{lr}
\begin{tikzpicture}
    \begin{scope}
        \clip \firstcircle;
        \fill[filled] \secondcircle;
    \end{scope}
    \draw[outline] \firstcircle node {$E$};
    \draw[outline] \secondcircle node {$F$};
    \node[anchor=south] at (current bounding box.north) {$E \cap E$};
\end{tikzpicture} 
&
\begin{tikzpicture}
    \draw[filled] \firstcircle node {$E$}
                  \secondcircle node {$F$};
    \node[anchor=south] at (current bounding box.north) {$E \cup F$};
\end{tikzpicture}
\end{tabular}
\end{center}

## Différence symétrique et soustraction
\def\firstcircle{(0,0) circle (1.1cm)}
\def\secondcircle{(0:1.4cm) circle (1.1cm)}

\colorlet{circle edge}{blue!50}
\colorlet{circle area}{blue!20}

\tikzset{filled/.style={fill=circle area, draw=circle edge, thick},
    outline/.style={draw=circle edge, thick}}

\begin{center}
\begin{tabular}{lr}
\begin{tikzpicture}
    \draw[filled, even odd rule] \firstcircle node {$E$}
                                 \secondcircle node{$F$};
    \node[anchor=south] at (current bounding box.north) {$\overline{E \cap F}$};
\end{tikzpicture}
&
\begin{tikzpicture}
    \begin{scope}
        \clip \firstcircle;
        \draw[filled, even odd rule] \firstcircle node {$E$}
                                     \secondcircle;
    \end{scope}
    \draw[outline] \firstcircle
                   \secondcircle node {$F$};
    \node[anchor=south] at (current bounding box.north) {$E - F$};
\end{tikzpicture}
\end{tabular}
\end{center}

# Produit cartésien et relations

## Couples d'éléments
On note $(x, y)$ l'ensemble $\{\{x\}, \{x, y \} \}$ (couple de Kuratowski).

On peut montrer que $(x, y) = (x', y') \iff (x = x' \mbox{ et } y = y')$.

## Produit cartésien
On note $E\times F = \{ (x, y) : x\in E \mbox{ et } y\in F \}$ le 
_produit cartésien_ de $E$ et $F$.

## Relations
Une relation $\mathcal{R}$ entre éléments se définit comme 
sous-ensemble d'un produit cartésien :
pour $x\in E$ et $y\in F$, $\mathcal{R} \subset E\times F$.

$$ x \mathcal{R} y \iff (x, y) \in \mathcal{R} $$

On dit parfois que le sous-ensemble de $E\times F$ est le _graphe_ de
la relation (en fait _c'est_ $\mathcal{R}$). On parle de relation sur
$E$ si $E = F$.

# Relations particulières

## Relations d'équivalence ($E = F$)
- Réflexive : $x\mathcal{R}x$
- Symétrique : $x\mathcal{R}y \Rightarrow y\mathcal{R}x$
- Transitive : $x\mathcal{R}y \mbox{ et } y\mathcal{R}z
  \Rightarrow x\mathcal{R}z$

## Relations d'ordre ($E = F$)
- Réflexive : $x\mathcal{R}x$
- Antisymétrique : $x\mathcal{R}y \mbox{ et } y\mathcal{R}x 
  \Rightarrow x = y$
- Transitive : $x\mathcal{R}y \mbox{ et } y\mathcal{R}z
  \Rightarrow x\mathcal{R}z$

## Fonctions
Une relation $\mathcal{R} \subset E\times F$ est une fonction ssi
pour tout $x\in E$ il existe _un et un seul_ couple $(x, y) \in \mathcal{R}$.

On note alors : $y = f(x) \iff x\mathcal{R} y$

# Note historique

##
Historiquement, in France (suivant le groupe de mathématiciens Bourbaki)
on admet souvent une définition légèrement différente :

- Une _fonction_ est une relation telle que, pour $x$ donné, 
  il existe _au plus_ un $y$ 
  tel que $x\mathcal{R}y$
- S'il en existe _un et un seul_ on parle d'_application_
- Il peut donc alors exister $x\in E$ tel que $y = f(x)$ n'existe pas
- Si l'on _restreint_ $f$ au _domaine de définition_ $\mathcal{D}_f \subset E$
   de $f$ on obtient une _application_

## 
La pratique internationale usuelle parle de fonction uniquement si
il existe _un et un seul_ $y$ tel que $x\mathcal{R}y$.

_Nous suivrons cet usage._

# Exercices

##
Prouvez que $E\times F = \varnothing \iff E = \varnothing \mbox{ ou } F = \varnothing$.

##
Soit $\mathcal{R}$ une relation d'ordre sur $E$. Comment décrire la
relation d'_ordre_ $\mathcal{R}'$ _« inverse »_ de $\mathcal{R}$ ?

##
Soit $\mathcal{R}$ une relation d'équivalence sur $E$. 

Pour $x\in E$ on note $\overline{x} = \{ y : x\mathcal{R}y \}$.

Montrez que $x\mathcal{R}y \iff \overline{x} = \overline{y}$.

On dit que $A, B, C, \ldots$ forment une _partition_ de $E$ ssi ils sont
disjoints
deux à deux ($A\cap B = A\cap C = B\cap C \ldots = \varnothing$) et recouvrent $E$ ($A \cup B \cup C \ldots = E$)

Montrez que les ensembles $\overline{x}$ forment une _partition_ de E.

Note : on appelle $\overline{x}$ la _classe d'équivalence_ de $x$.

# Propriétés des fonctions

##
Si $f \subset E\times F$ est une fonction, on dit aussi que $f$ est 
une fonction de $E$ _vers_ $F$. Si $y=f(x)$ on dit que y est l'_image_
de $x$ par $f$ et que $x$ est un _antécédent_ de $y$.

## Surjectivité
Une fonction surjective est telle que tout élément de $F$ a _au moins_
un antécédent dans $E$.

## Injectivité
Une fonction injective est telle qu'un élément de $F$ a _au plus_
un antécédent dans $E$ : $f(x) = f(y) \Rightarrow x=y$

## Bijectivité
Une fonction bijective est injective et bijective.

# Construction des nombres naturels

## Giuseppe \textsc{Peano} (1889)
Il existe un ensemble $\mathbb{N}$ (et un élément 0 et une
fonction $S$ de $\mathbb{N}$ dans $\mathbb{N}$) tels que
(en notant $Sn$ pour $S(n)$) :

- $0 \in \mathbb{N}$
- Pour tout $n \in \mathbb{N}$, il existe $Sn \in \mathbb{N}$
  (successeur de $n$)
- Il n'y a aucun élément $n$ de $\mathbb{N}$ tel que $Sn = 0$
- Si $Sn = Sm$ alors $n = m$ (S est _injective_)
- Si $E \subset \mathbb{N}$, $0\in E$ et ($n\in E \Rightarrow Sn\in E$) alors 
  $E = \mathbb{N}$

##
Intuitivement : On a zéro, le successeur est le nombre suivant, 0 n'est
le successeur d'aucun nombre, deux nombres différents ont toujours des
successeurs différents et... le principe de récurrence.

# Opérations

## Définitions
- On peut définir l'addition :
  * _(a0)_ $n + 0 = n$
  * _(a1)_ $x + Sy = S(x + y)$
- ... et la multiplication :
  * _(m0)_ $n\times 0 = 0$
  * _(m1)_ $x\times Sy = (x\times y) + x$

##
$\;\;\;\;S0 + S0$

$= S(S0 + 0)$ : application règle _(a1)_

$= S(S0) = SS0$ : application règle _(a0)_

##
C'est une démonstration de :
$$1 + 1 = 2$$
\begin{flushright}
Étonnant, non ?
\end{flushright}

# Exercices

##
On note $1 = S0$, $2 = SS0$, $3 = SSS0$, $4 = SSSS0$,

$5 = SSSSS0$, $6 = SSSSSS0$

Prouvez (en indiquant les règles appliquées à chaque étape) que :

$$2 + 2 = 4$$
$$2\times 3 = 6$$

##
Prouvez les propriétés suivantes de $+$ et $\times$ :

- Commutativité : $a + b = b + a$, $a \times b = b \times a$
- Associativité : $(a + b) + c = a + (b + c)$
  et $(a\times b)\times c = a\times(b\times c)$
- Distributivité de $\times$ sur $+$ : $a\times (b + c) = (a\times b) + (a\times c)$

##
Définissez une relation d'ordre « naturelle » sur $\mathbb{N}$ sous forme
de règles similaires à celles de l'addition et de la multiplication.

# Entiers de Von Neumann

## Un _modèle_ satisfaisant les axiomes de Peano
- $0 = \varnothing$
- Pour un ensemble $x$, $S(x) = x \cup \{x\}$
- $\mathbb{N}$ est l'intersection de tous les ensembles $E$ contenant $0$ et
  clos pour $S$ ($x \in E \Rightarrow Sx \in E$)

## Exercices
- Ecrivez les nombres de 0 à 4 sous forme d'ensembles selon ce modèle
- Implémentez les entiers de Von Neumann sous forme d'une classe en Python.

# Entiers de Zermelo

## Un autre modèle satisfaisant les axiomes de Peano
- $0 = \varnothing$
- $S(x) = \{x\}$

## Exercices
- Écrivez les nombres de 0 à 4 sous forme d'ensembles selon ce modèle
- Implémentez les entiers de Zermelo sous forme d'une classe en Python.

# Retour sur les classes d'équivalences

## 
On a vu que si $\mathcal{R}$ est une relation d'équivalence sur un
ensemble $E$ l'ensemble des classes d'équivalence
$\overline{x} = \{ y \in E : x\mathcal{R}y \}$ forment une _partition_ de
$E$.

## 
Un élément d'une _classe d'équivalence_ est appelé un _représentant_ de
cette classe.

##
On appelle l'ensemble des classes d'équivalence pour la relation $\mathcal{R}$
l'ensemble _quotient_ de $E$ par $\mathcal{R}$, noté $E/\mathcal{R}$

## Exemples
- Classes d'équivalence de droites parallèles du plan : _directions_
- Classes d'équivalence de bi-points équipollents (_i.e._
  $(A, B)$ et $(C, D)$ forment un parallèlogramme $ABDC$) : _vecteurs_

# Opérations et classes d'équivalences

## Compatibilité

Étant donnée une relation d'équivalence $\sim$.

Si une opération _binaire_ interne $\ast$ sur $E$ 
(c'est-à dire une fonction de $E\times E$ dans $E$) vérifie :

$$ \forall (x,y) \in E\times E\;\;\; \forall x' \in \overline{x}\;\;
\forall y' \in \overline{y} : x'\!\! \ast y' \in \overline{x \ast y} $$

On dit que la relation $\ast$ est _compatible_ avec $\sim$.

##
Autrement dit : si on applique $\ast$ à deux représentants quelconques
de leurs classes d'équivalence respectives, le résultat est 
toujours dans la même classe d'équivalence.

##
L'opération $\ast$ permet donc de définir naturellement une
opération (qu'on notera de la même façon) sur $E/\!\sim$.

# Construction des nombres relatifs

## Classes d'équivalence de couples d'entiers naturels
$\mathbb{Z} = \mathbb{N}\times \mathbb{N}/\!\sim$ (ensemble quotient des 
couples d'entiers naturels par la relation d'équivalence $\sim$) où $\sim$
est définie par :
$$ (a, b) \sim (c, d) \iff a + d = b + c $$

##
$+$ et $\times$ étant définis ainsi :
$$ (a, b) + (c, d) = (a + c, b + d)$$
$$ (a, b)\times (c, d) = (a\times c + b\times d, a\times d + b\times c)$$

Ces définitions sont compatibles avec la relation $\sim$.

##
$n\in \mathbb{N}$ est identifié à la classe d'équivalence de $(n, 0)$.

Les classes d'équivalence qui contiennent $(0, n)$ sont notées $-n$.

# Construction des nombres rationnels

## Classes d'équivalence de couples d'entiers relatifs
$\mathbb{Q} = \mathbb{Z}\times \mathbb{Z}^{\ast}/\!\sim$ (ensemble quotient des 
couples d'entiers relatifs par la relation d'équivalence $\sim$) où $\sim$
est définie par :
$$ (a, b) \sim (c, d) \iff a\times d = b\times  c $$

##
$+$ et $\times$ étant définis ainsi :
$$ (a, b) + (c, d) = (a\times d + b\times c  , b\times d)$$
$$ (a, b)\times (c, d) = (a\times c, b\times d)$$

Ces définitions sont compatibles avec la relation $\sim$.

##
$n \in \mathbb{Z}$ est identifié à la classe d'équivalence de $(n, 1)$.

Les classes d'équivalence qui contiennent $(n, m)$ sont notées
$\displaystyle \frac{n}{m}$


# Construction des nombres réels

## Couples d'intervalles particuliers de $\mathbb{Q}$
$(A, B) \in \mathcal{P}(\mathbb{Q})\times \mathcal{P}(\mathbb{Q})$
tels que :

1. $A\neq \varnothing$, $B\neq \varnothing$
2. $A \cup B = \mathbb{Q}$
3. $\forall q\in A, \forall r\in B : q < r$ 

 N.B. Ces trois premières conditions impliquent que la connaissance
  de $A$ détermine celle de $B$ (et réciproquement) :
  $B = \mathbb{Q}\backslash A$ et $A = \mathbb{Q}\backslash B$

4. Si $B$ à une _borne inférieure_ dans $\mathbb{Q}$, elle appartient à $B$

  La borne inférieure d'un ensemble $B$ est le plus _grand_ des _minorants_
  (éléments plus petits que tout élément de $B$), si il existe.

##
Ce sont des _coupures de Dedekind_ (1858) qui définissent ainsi
l'ensemble $\mathbb{R}$ des nombres _réels_.

# Exemples de coupures de Dedekind

##
Un nombre rationnel $r$ est identifié à :
$$ A =\; ]-\infty, r[ \;\;\mbox{ et donc } B = [r, +\infty[ $$

\begin{center}
$\displaystyle\sqrt{7}$ est
$\vspace{2mm}\displaystyle A = \left\{ \frac{p}{q} \in \mathbb{Q} : p^2 < 7 q^2 \right\}$

$\displaystyle\sqrt[3]{2}$ est 
$\displaystyle A = \left\{ \frac{p}{q} \in \mathbb{Q} : p^3 < 2 q^3 \right\}$
\end{center}

##
Une autre construction, équivalente, à partir des suites de nombres rationnels,
existe : classes d'équivalence de _suites de Cauchy_.

Intuitivement : un nombre réel est la classe des suites de rationnels
qui « convergent » vers lui.

##
Ce n'est pas une définition circulaire
car le _critère de Cauchy_ permet de définir une sorte de convergence
sans qu'il n'existe nécessairement une valeur limite.

# Construction des nombres complexes

##
Cette fois on part de l'anneau des _polynômes_ à une variable et
à coefficients dans $\mathbb{R}$ :
$$ \mathbb{R}[X] = \left\{ a_{n}X^n + a_{n-1}X^{n-1} + ... + a_{1}X + a_0 :
\;  a_i \in \mathbb{R} \right\} $$

##
La relation d'équivalence $\sim$ sur $\mathbb{R}[X]$ est définie par :
$$ P \sim Q \iff P = Q \;\;\;\; \left[ X^2 + 1 \right] $$
_i.e._ le reste de la division euclidienne de $P$ et $Q$ par $X^2 + 1$
est le même, ou encore $P - Q$ est un multiple du polynôme $X^2 + 1$.  

## Voilà donc le _corps_ des nombres complexes

$$ \mathbb{C} = \mathbb{R}[X]/\!\sim\; = \mathbb{R}[X]/\left(X^2 + 1\right) $$

# Quelques nombres complexes

##
Un nombre réel $a$ est identifié à la classe d'équivalence du
polynôme constant $a$.

##
$i$ est la classe d'équivalence du polynôme $X$.
Tout nombre complexe peut s'écrire $a + ib$ où $(a, b)\in\mathbb{R}^2.$

$$ i = \overline{X} = \left\{ P\in \mathbb{R}[X] : P = X\;\; [X^2 + 1]\right\} $$

##
$$ X^2 = - 1\;\; \left[ X^2 + 1 \right] \Rightarrow -1\in\overline{X^2} \Rightarrow i^2 = -1 $$

##
Une telle construction, due au jeune Évariste \textsc{Galois} (1811--1832), fonctionne
avec n'importe quel polynôme irréductible sur un corps quelconque. On
peut, par exemple, en partant du polynôme (dans $\mathbb{Q}[X]$) $X^2 - 2$
construire
$\mathbb{Q}[\sqrt{2}] =$ $\{ a + b\sqrt{2} : (a, b)\in\mathbb{Q}^2 \}$.

# Et en informatique ?

##
L'électronique étant généralement _binaire_ (ce n'est pas la seule
possibilité) les nombres entiers sont généralement représentés dans cette 
base, et s'ils sont signés (peuvent être négatifs) plusieurs approches
sont possibles :

- Stocker le signe à part (rare)
- Complément à 1
- Complément à 2 (le plus courant)

On ne représente pas _entièrement_ $\mathbb{N}$ !
En complément à 2, par exemple, on est limité à l'intervalle
$[-2^{63}, 2^{63} - 1]$ (en 64 bits).

##
Les nombres décimaux sont stockés selon le même principe avec 
_mantisse_ (chiffres binaires « après la virgule ») et _exposant_.

##
Pour les traitements comptables et financiers on préfèrera le
_Décimal codé binaire_ (DCB en français, BCD en anglais).

# Quelques surprises...

## En Python, mais pas seulement... Attention à _float_ !

~~~~Python
>>> 1/10 + 2/10 == 3/10
False
>>> from decimal import Decimal
>>> Decimal(0.1) + Decimal(0.2) == Decimal(0.3)
False
>>> Decimal('0.1') + Decimal('0.2') == Decimal('0.3')
True
>>> from fractions import Fraction
>>> Fraction(1,10) + Fraction(2,10) == Fraction(3,10)
True
~~~~

## En Python les entiers n'ont pas de limitation
~~~~Python
>>> print(2**63, 2**63 + 1)
9223372036854775808 9223372036854775809
~~~~

# D'autres surprises...

## PHP

~~~~PHP
<?php
print(2**62); print("\n"); print(2**63);
?>

4611686018427387904
9.2233720368548E+18
~~~~

## JavaScript

~~~~JS
console.log(9223372036854775806)

9223372036854776000
~~~~

# C

## Parlons un peu à la machine (presque) directement...

~~~~C
#include<stdio.h>
#include<stdlib.h>

int main() {
  int     n1 = 2147483647;          /* 2**31 - 1 */
  int64_t n2 = 9223372036854775807; /* 2**63 - 1 */
  printf("%+d\t%+lld\n", n1, n2);
  printf("%+d\t%+lld\n", n1 + 1, n2 + 1);
  exit(0);
}
~~~~

##
~~~~
$ make && ./maxi
cc -o maxi maxi.c
+2147483647     +9223372036854775807
-2147483648     -9223372036854775808
~~~~

# Autres classes de nombres

##
D'autres types de nombres exotiques ont été définis par des
mathématicien·e·s et peuvent avoir des applications en physique
ou informatique.

##
- Quaternions (comme les complexes mais avec quatre composantes
  réelles). Il sont utilisés en représentation de l'espace 3D (robotique,
  images de synthèse, jeux vidéos)
- Octonions

##
- Nombres _surréels_ de Conway, utiles en _théorie des jeux_
- Infinitésimaux en calcul différentiel et intégral (analyse
  _non-standard_)

##
- Nombres _p-adiques_ : nombre de chiffres infini en base $p$,
$p$ étant un nombre premier.

# Nombres cardinaux

## Définition
Soient deux ensembles $E$ et $F$, on dit qu'ils ont le même cardinal
ssi il existe une _fonction bijective_ de E dans F.

On note : $\mathbf{card}(E) = \mathbf{card}(F)$ ou encore $|E| = |F|$

## Examples

- Le cardinal d'un ensemble _fini_ s'identifie à un entier naturel
- Tout ensemble a le même cardinal que lui-même ($f = Id$)
- L'ensemble des nombres entiers pairs a le même cardinal que celui de 
  tous les nombres entiers
- De même que les ensembles des nombres impairs, carrés, premiers, ...
- L'ensemble des rationnels a le même cardinal que celui de l'ensemble
  des nombres entiers : $|\mathbb{N}| = |\mathbb{Q}|$ mais celui de
  l'ensemble des réels est « plus grand » : $|\mathbb{N}| < |\mathbb{R}|$
- Deux segments de droite ont le même cardinal

# Ordinaux

## Bon ordre
Un ensemble ordonné $(E, \leqslant)$ est dit _bien ordonné_ si tout
sous-ensemble de $E$ admet un plus petit élément.

##
Deux ensembles _bien ordonnés_ $(E, \leqslant)$ et $(F, \leqslant')$
ont le même _ordinal_ si il
existe une fonction bijective $f$ de $E$ dans $F$ préservant les
relations d'ordre sur $E$ et $F$ :

$$ x \leqslant y \iff f(x) \leqslant' f(y) $$

Un _nombre ordinal_ peut se concevoir comme la classe d'équivalence d'ensembles
partageant un même ordinal.

##
N.B. Cette définition n'est pas tout à fait bien fondée, les classes en
question étant plus grandes que des ensembles dans la plupart des
schémas d'axiomes. Voir la construction de Von Neumann.

# Ensembles finis et infinis

##
Il a longtemps été tenu comme problématique de considérer des
collections infinies comme un tout : du fait qu'elles peuvent
être mises en correspondance (bijection) avec une partie stricte
d'elles-mêmes.

## 
C'est la définition _positive_ des ensembles infinis depuis
la fin du XIXe siècle.

- Entiers naturels / nombres pairs : $n \rightarrow 2n$
- Segment de droite / moitié du segment : $x \rightarrow x/2$
- Demi-droite / segment : $x \rightarrow \frac{1}{x}$
- Points d'un cercle, d'un disque, ...

##
Les ensembles _finis_ sont ceux où une telle bijection entre
$E$ et un sous-ensemble strict $F$ de $E$ ($F\subset E, F \neq E$)
n'existe pas.

Leurs cardinaux s'identifient à un entier naturel, leurs ordinaux aussi.

# Ensembles dénombrables

## 
Un ensemble infini est _dénombrable_ s'il peut être mis en bijection
avec $\mathbb{N}$. On note son _cardinal_ $\aleph_0$.

Il n'existe pas d'ensemble infini _plus petit_ que $\mathbb{N}$ 
_i.e._ tout ensemble infini contient un sous-ensemble dénombrable.

Attention : dans certains contextes _dénombrable_ inclut aussi les
ensemble _finis_.

## Exemples
- Nombres pairs, impairs, premiers, carrées, relatifs, ...
- Paires de nombres entiers $(a, b)$
- n-uplets de nombres entiers $(a, b, c, ...)$
- Nombres rationnels
- Mots (en théorie des langages) construits sur un alphabet fini
- Programmes d'une machine de Turing
- Programmes dans un langage donné
- Théorèmes d'une théorie formelle


# Parties d'un ensemble

## Ensemble des sous-ensembles
On note $\mathcal{P}(E)$ l'ensemble des sous-ensembles de $E$.

Appelé aussi _ensemble des parties_ de $E$ (qui contient, entre
autres, $\varnothing$ et $E$ lui-même).

## Fonction indicatrice
Pour $F \subset E$ (_i.e._ $F \in \mathcal{P}(E)$), on note $\mathbbm{1}_F$
la fonction de $E$ vers $\{0, 1\}$ définie par :

\begin{center}
\begin{math}
\mathbbm{1}_F(x) = \left\{
    \begin{array}{ll}
        1 & \mbox{si } x \in F \\
        0 & \mbox{si } x \notin F 
    \end{array}
\right.
\end{math}
\end{center}

##
Inversement si $f$ est une fonction de $E$ vers $\{0, 1\}$, elle
est la fonction indicatrice de l'ensemble :

$$ \{ x \in E : f(x) = 1\} \in \mathcal{P}(E)$$

# Argument diagonal

## $\mathbb{R}$ n'est pas _dénombrable_
On montre qu'une _partie_ de $\mathbb{R}$, l'intervalle $[0,1[$ n'est pas
dénombrable (Cantor).

S'il l'était on pourrait constituer une suite de tous les réels de 
$[0,1[$ sous forme décimale :

##
$$
\begin{array}{ccccccccc}
x_1 = 0, & \mathbf{d_{11}} & d_{12} & d_{13} & d_{14} & d_{15} & d_{16} & d_{17} & \ldots \\
x_2 = 0, & d_{21} & \mathbf{d_{22}} & d_{23} & d_{24} & d_{25} & d_{26} & d_{27} & \ldots \\
x_3 = 0, & d_{31} & d_{32} & \mathbf{d_{33}} & d_{34} & d_{35} & d_{36} & d_{37} & \ldots \\
x_4 = 0, & d_{41} & d_{42} & d_{43} & \mathbf{d_{44}} & d_{45} & d_{46} & d_{47} & \ldots \\
x_5 = 0, & d_{51} & d_{52} & d_{53} & d_{54} & \mathbf{d_{55}} & d_{56} & d_{57} & \ldots \\
x_6 = 0, & d_{61} & d_{62} & d_{63} & d_{64} & d_{65} & \mathbf{d_{66}} & d_{67} & \ldots \\
x_7 = 0, & d_{71} & d_{72} & d_{73} & d_{74} & d_{75} & d_{76} & \mathbf{d_{77}} & \ldots \\
\vdots   &&&&&&&& \ddots
\end{array}
$$

# Argument diagonal

##
À partir de la _diagonale_ des _chiffres_ placés à la 
$n^{\mbox{\tiny e}}$ décimale de $x_n$.

On peut contruire un réel, _antidiagonal_, $r \in [0,1[$ :
$$
r = 0,\delta_1 \delta_2 \delta_3 \ldots \delta_n \ldots 
$$
$$
\mbox{où } \delta_n = \left\{
    \begin{array}{ll}
        5 &\mbox{si } d_{nn} \neq 5 \\
        0 &\mbox{si } d_{nn} = 5 
    \end{array}
    \right.
\mbox{ donc } \delta_n \neq d_{nn}
$$

##
Pour tout $n \geqslant 1, r \neq x_n$

Car $r$ et $x_n$ diffèrent, au moins, sur leur $n^{\mbox{\tiny e}}$ décimale.

$r$ n'est donc pas dans la liste, une telle liste n'existe donc pas.

## 
Un argument similaire permet de prouver que $|\mathcal{P}(E)|$ > $|E|$
ou encore qu'il n'existe pas de programme capable de préduire l'arrêt de tout autre
programme en connaissant son entrée.

# Exercices

##
Combien un ensemble _fini_ de taille $n$ a-t-il de parties ?

## 
Montrer que pour tout ensemble (fini ou infini), il n'existe pas
de bijection entre lui et l'ensemble de ses parties.

En conclure que $|\mathcal{P}(E)| > |E|$.

## Conclusion
Il existe donc des ensembles infinis strictement plus grand
que $\mathbb{N}$, _i.e. non-dénombrables_ (cardinaux $> \aleph_0$):

- $\mathcal{P}(\mathbb{N})$ : cardinal $\aleph_1$
- $\mathbb{R}$, $\mathbb{R}^n$

Et, au-delà, des ensembles infinis de _« plus en plus grands »_ :

- $\mathcal{P}(\mathcal{P}(\mathbb{N})), \mathcal{P}(\mathcal{P}(\mathcal{P}(\mathbb{N}))), \ldots$
- Dont les cardinaux sont notés $\aleph_2, \aleph_3, ...$
- _L'infini_ (ScienceÉtonnante) :
\url{https://www.youtube.com/watch?v=1YrbUBSo4Os}

# 

\begin{small}
\emph{The infinite we shall do right away. The finite may take a little longer.
}
\begin{flushright}
--- Stanislaw \textsc{Ulam}
\end{flushright}
\end{small}

\begin{small}
\emph{La nature de l'infini est telle que des pensées finies ne le sauraient comprendre.}
\begin{flushright}
--- Descartes, \emph{Principes}
\end{flushright}
\end{small}


# Suites

##
Une suite d'éléments d'un ensemble $E$ est une fonction de $\mathbb{N}$
dans $E$ définie à partir d'un certain rang (généralement 0 ou 1).

## Limite ensembliste
Si les éléments d'une suite sont des ensembles on peut définir les
limites respectivement  _inférieure_  et _supérieure_ :

$$\underline{\lim}\;E_n = \bigcup_{n\geqslant 0}\bigcap_{k\geqslant n} E_k$$
$$\overline{\lim}\;E_n = \bigcap_{n\geqslant 0}\bigcup_{k\geqslant n} E_k$$

##
Si $\underline{\lim}\;E_n = \overline{\lim}\;E_n$ alors on dit que la suite
converge au sens de la théorie des ensembles et, par définition : 
$$\lim E_n = \underline{\lim}\;E_n = \overline{\lim}\;E_n$$

# Limite ensembliste

##
Intuitivement, la limite au sens de la théorie des ensembles concerne
ce dont parle la théorie : appartenance ou non.

##
Un élément est membre de la _limite ensembliste_ (si elle existe)
dès lors qu'il est _acquis_ à un certain rang de la suite et
conservé ensuite par _tous_ les termes suivants de la suite.

##
Cette limite peut donc facilement se retrouver _vide_.

Si un élément est régulièrement _acquis_, puis _perdu_, puis
encore _acquis_ ensuite, et ainsi de suite, il sera membre de
la limite supérieure mais _pas_ de la limite inférieure.

La fonction indicatrice va alors _osciller_ entre $0$ et $1$
pour cet élément.

##
Les calculs de _complexité algorithmique_ peuvent nécessiter
des raisonnements portant sur la limite ensembliste.

# Limite ensembliste

##
_Théorème_ : Si la limite de la suite des fonctions indicatrices
existe, la fonction limite obtenue est la fonction indicatrice de
l'ensemble limite au sens précédent.

Les fonctions indicatrices ne peuvent prendre que deux valeurs
en chaque point ($0$ ou $1$), donc _si_ une telle limite existe
c'est que, _en chaque point_,  les valeurs prises en un point 
ne changent plus à partir d'un certain terme (cf.
_On Ducks and Bathtubs_, \url{http://bsb.me.uk/dd-wealth.pdf},
par Ben Bacarisse).

## Exercice
Soit $F_n = \{ 0, ..., n \} = \{ k \in \mathbb{N} : k\leqslant n \}$.

Déterminer, si elle existe, la limite
ensembliste  de la suite $(F_n)$.

- Vous pouvez la déterminer à partir des formules des limites supérieure
et inférieure.
- Vous pouvez aussi la déterminer en considérant la limite de la
suite des fonctions indicatrices des ensembles $F_n$.

# Solution de l'exercice

## Première version (limites supérieure et inférieure)
Pour tout $n \in \mathbb{N}$ : 
$$\bigcap_{k\geqslant n} F_k = \{0,..., n\} \cap \{0,...,n+1\} \cap \ldots = \{0,...,n\} = F_n \mbox{ donc :}$$

\vspace{-1cm}
$$\underline{\lim}\;F_n = \bigcup_{n\geqslant 0}\overbrace{\bigcap_{k\geqslant n} F_k}^{F_n} = \bigcup_{n\geqslant 0} F_n = \{0\} \cup \{0, 1\} \cup \{0, 1, 2\} \cup \ldots = \mathbb{N}$$


\vspace{-0.5cm}
$$\mbox{Pour tout } n\in\mathbb{N} : \bigcup_{k\geqslant n} F_k = \{0,..., n\} \cup \{0,...,n+1\} \cup \ldots = \mathbb{N} \mbox{ donc :}$$

\vspace{-0.7cm}
$$\overline{\lim}\;F_n = \bigcap_{n\geqslant 0}\overbrace{\bigcup_{k\geqslant n} F_k}^{\mathbb{N}} = \bigcap_{n\geqslant 0}\mathbb{N} = \mathbb{N} \cap \mathbb{N} \cap \ldots = \mathbb{N} \hspace{3cm}$$ 

Les limites supérieure et inférieure étant égales à $\mathbb{N}$,
la suite $(F_n)$ a une limite qui est $\mathbb{N}$. 

# Solution de l'exercice

## Seconde version (fonctions indicatrices)
Soit $\mathbbm{1}_{F_n}$ la fonction indicatrice de l'ensemble $F_n$

\begin{center}
\begin{math}
\mathbbm{1}_{F_n}(k) = \left\{
    \begin{array}{ll}
        1 & \mbox{si } k \leqslant n \\
        0 & \mbox{si } k > n 
    \end{array}
\right.
\end{math}
\end{center}

Donc, pour tout $k \in \mathbb{N}$ :
$$\mathbbm{1}_{F_n}(k) = 1 \mbox{ si } n \geqslant k $$

$$\mbox{donc }\forall k\in \mathbb{N} : \lim_{n\to +\infty} \mathbbm{1}_{F_n}(k) = 1$$
la suite de fonctions $(\mathbbm{1}_{F_n})$ converge donc (au sens
des fonctions) vers la fonction constante : $k\rightarrow 1$, qui est la
fonction indicatrice $\mathbbm{1}_{\mathbb{N}}$ de $\mathbb{N}$.

La suite $(F_n)$ a donc comme limite $\mathbb{N}$. 

# Arithmétique modulaire

##
Ou _arithmétique de l'horloge_ : on réalise les calculs et on ne
garde que le reste de la division euclidienne par un certain entier
$n$ (calculs _« modulo $n$ »_), l'ensemble de travail est _fini_.
On constate que l'addition et la multiplication fonctionnent 
presque comme d'habitude.

##
Par exemple : $3\times 4 + 7 = 1\;\;[6]$, mais $3\times 2 = 0\;\;[6]$
(diviseurs de zéro !)

##
Si $p$ est un nombre premier, alors il n'y a pas de diviseurs de
zéro. $\mathbb{Z}/p\mathbb{Z}$ est alors un _corps_ : même propriétés
algébriques essentielles 
que $\mathbb{R}$ et $\mathbb{C}$. Sinon c'est seulement un _anneau_.

## 
On peut cependant construire des corps finis à $p^n$ éléments si
$p$ est premier.

# Applications des calculs modulaires

##
Dès qu'un système de comptage _boucle_, les calculs modulaires sont
utiles : _temps_, _dates_, _rotations_, etc.

##
Dès que l'on travaille sur des nombres stockés en espace fixe
en mémoire on travaille de cette façon.

## Cryptographie
- La plupart des algorithmes de chiffrement, symétriques ou non,
  de calcul de _sommes de contrôle_ les utilisent
- Dans SSL : RSA, Diffie-Hellman 
- Sommes MD5, SHA, etc.


# Dénombrement

##
La combinatoire est l'art de compter combien il existe de structures
construites à partir d'ensembles _finis_.

- Nombre de parties d'un ensemble à $n$ éléments : $2^n$
- Nombre de _permutations_ de $n$ objets, factorielle de $n$ :
$n! = 1\times 2\times ... \times n$ et $0! = 1$

- Nombre de _combinaisons_ de $p$ objets parmi $n$ :
$${\mathrm C}_n^p = \binom{n}{p} = \frac{n!}{p!(n-p)!}$$
- Nombre d'_arrangements_ de $p$ objets parmi $n$ :
$${\mathrm A}_n^p = P(n, p) = n\times (n-1)\times (n-2)\times (n-p+1)$$
$$ {\mathrm A}_n^p = p!\binom{n}{p} = \frac{n!}{(n-p)!}$$

# Formule du binôme

## Newton

$$ (a + b)^2 = a^2 + 2ab + b^2 $$

$$ (a + b)^3 = a^3 + 3a^2b + 3ab^2 + b^3 $$

$$ (a + b)^4 = a^4 + 4a^3b + 6a^2b^2 + 4ab^3 + b^4 $$

$$ \cdots $$

##
$$ (a + b)^n = \sum_{k=0}^{n} \binom{n}{k} a^{n - k} b^{k} $$

# Facile à retrouver  !

##
$$ \binom{n}{k} = \binom{n - 1}{k - 1} + \binom{n - 1}{k} $$

## Triangle de Pascal
$$
\begin{tikzpicture}
\foreach \n in {0,...,4} {
  \foreach \k in {0,...,\n} {
    %\node at (2*\k-\n,-\n) {${\n \choose \k} = \mathbf{ \binomialb\n\k$} };
    \node at (2*\k-\n,-\n) {${\binom{\n}{\k}} = \mathbf{\binomialb\n\k}$};
  }
}
\end{tikzpicture}
$$

# En Python

## 
Le module _itertools_ permet de générer des permutations,
des combinaisons, sans et avec remise, et génère des itérables.

##
~~~~Python
>>> from itertools import (product, permutations, 
         combinations, combinations_with_replacement,
         groupby)
>>> list(combinations( ['spam', 'ham', 'egg'], 2))
[('spam', 'ham'), ('spam', 'egg'), ('ham', 'egg')]
~~~~

##
Voir aussi le module _sympy.combinatorics_

## Exercice
Écrire une fonction qui renvoie les _arrangements_
de $p$ objets parmi une liste. Indice : combinez combinations
et permutations, pensez à utiliser éventuellement _chain_.

# Pour juste les compter

## 
Calculez efficacement les nombres de combinaisons,
d'arrangements, de permutations, etc. ? Dans le module _math_ :
_comb_ et _factorial_. Voir aussi le module _scipy.special.comb_.

##
~~~~Python
from math import factorial, comb
def k_perm(n, p):
    '''Calcule le nombre d'arrangements
    de p objets parmi n.'''
    return factorial(p)*comb(n,p)
~~~~

# Probabilités

\renewcommand{\thefootnote}{\fnsymbol{footnote}}

## Axiomatisation de Kolmogorov (1933) 
Soit un ensemble $\Omega$ appelé _univers_. Une probabilité sur 
$\Omega$ c'est une fonction P de $\mathcal{P}(\Omega)$
vers $[0, 1]$
\footnote[1]{Plus rigoureusement d'une \emph{tribu} sur $\Omega$
vers $[0, 1].$}.
 Qui vérifie :

- $P(\Omega) = 1$ (la probabilité de l'univers vaut 1)
- Si $A_1, A_2, ...$ sont disjoints deux à deux (pour tout $i, j$, si $i \neq j$ 
  alors $A_i \cap A_j = \varnothing$) alors (additivité) :

$$ P\left(\bigcup_{i\geqslant 1} A_i\right) = \sum_{i\geqslant 1} P(A_i) $$

##
Un évènement est un sous-ensemble de $\Omega$, deux évènements
$A$ et $B$ sont dits indépendants ssi :

$$ P(A\cap B) = P(A)\times P(B) $$

# Probabilités conditionnelles

##
Si $A$ et $B$ sont deux évènements, $B$ de probabilité non-nulle, la
_probabilité conditionnelle de A sachant B_ est :

$$ P(A | B) = \frac{P(A\cap B)}{P(B)} $$

Si $A$ et $B$ sont indépendants $P(A | B) = P(A)$.

## Théorème de Bayes
Si $P(A)$ et $P(B)$ sont non-nulles :
$$ P(A | B) = \frac{P(B | A)P(A)}{P(B)} $$

# Variables aléatoires

## 
Une variable aléatoire est une fonction $X$ est une fonction _mesurable_ de
$\Omega$ vers un _espace mesurable_ (généralement $\mathbb{R}$).

##
On peut définir une probabilité $P_X$ à partir de $X$ ($X^{-1}$ est la
fonction inverse de $X$) : $P_X(A) = P(X^{-1}(B)) = P(X \in B)$

Dans le cas d'une variable $X$ _non-discrète_ ($X(\Omega)$ ni fini, ni dénombrable),
si $P(a\leqslant X \leqslant b) = \int_a^b f(x)dx$ on appelle $f$ sa
_fonction de densité_.

## L'espérance de $X$ est :

- $\displaystyle E[X] = \sum_i x_i P(X=x_i)$ si $X$ est _discrète_ 
- $\displaystyle E[X] = \int_{-\infty}^{+\infty}xf(x)dx$ si $X$ est _continue_ 

On peut voir l'_espérance_ comme une moyenne en statistique, on peut aussi définir
l'écart-type, la variance, etc.


# Probabilités discrètes et combinatoire

## Probabilité uniforme
Si $\Omega$ est fini, si on définit $P$ ainsi, pour $x\in \Omega$ :

$$ P(\{x\}) = \frac{1}{|\Omega|} $$

Ça suffit pour définir $P$ sur tout $\mathcal{P}(\Omega)$, pour
$A \in \mathcal{P}(\Omega)$ :

$$ P(A) = \frac{|A|}{|\Omega|} $$ 

## Exemple
On lance deux dés : $\Omega = \{1, 2, 3, 4, 5, 6\}\times \{1, 2, 3, 4, 5, 6\}$.

$A$ : « La somme des tirages est paire »

$$ A = \{ (1,1), (1,3), ..., (2,5), ... \} $$


# Comment choisir $\Omega$ et $P$ ?

##
On lance deux pièces de monnaie. 
On se demande quelle est la probabilité que les deux pièces tombent
sur deux faces différentes...

##
Comment peut-on représenter une _épreuve_ (un tirage) ?
On peut en imaginer deux selon que les distingue les pièces ou pas.

$$
\begin{array}{c}
\{\; (F, F), (F, P), (P, F), (P, P)\;\} \\
\{\; \{F\}, \{F, P\}, \{P\}\; \} 
\end{array}
$$

##
Si la probabilité est uniforme sur ces deux univers, les résultats
sont-ils les mêmes ? Lequel est le bon ?

##
Ce n'est pas une question mathématique, seules l'intuition et l'expérience
peuvent nous fournir la réponse.

# Lambda-calcul

##
Créé par Alonzo \textsc{Church} (1903--1995), basé sur les notions,
_syntaxiques_, de _fonction_  et d'_application_.

##
Fonction identité :
$\lambda x.x$

Fonction qui renvoie une fonction constante : 
$\lambda x.(\lambda y.x)$

## Règles de substitution et de réduction
Substitution de variables dans un terme :
$$ (\lambda x.(\lambda y.x))[x=u] \mbox{ donne } (\lambda u.(\lambda y.u)) $$
Réduction :
$$ (\lambda x. xx)(\lambda y.y) \mbox { donne } (\lambda y.y)(\lambda y.y) $$ 

# Programmation et lambda-calcul

## C'est un langage de programmation !
Vrai = $\lambda ab. a$

Faux = $\lambda ab. b$

$\mbox{if-then-else } = \lambda buv.(b u v)$

## On peut construire les nombres entiers !
$0 = \lambda fx.x$

$1 = \lambda fx. fx$

$2 = \lambda fx. f(fx)$

$3 = \lambda fx. f(f(f(x)))$

$Sn = (\lambda nfx.f(n f x)).n$

## Et les opérations $+$ et $\times$ !
$n+ p = (\lambda npfx.n f (p f x)).n p$ 

$n\times p = (\lambda npf.n (p f)).n p$

# Lambda-calcul dans les langages informatiques

## Langages fonctionnels
- LISP et Scheme sont construits directement à partir du $\lambda$-calcul :

~~~~Scheme

      (define sum-squares
                  (lambda (x y)
                     (+ (* x x) (* y y)))) 
      (sum-squares 7 10) ; 149


~~~~

- CaML est construit sur le $\lambda$-calcul _typé_
- Clojure, Haskell, ...

## Concepts fonctionnels
- _lambdas_ de Python
- Fonctions anonymes de JavaScript

# Machines de Turing

## Alan \textsc{Turing} (1912--1954) définit une machine théorique

- Ruban de longueur infinie, découpé en cases
- Alphabet : symboles présents ou non sur une case (symbole _blanc_),
  on peut se ramener à 0/1/blanc (binaire) ou I/blanc (unaire)
- Tête de lecture : placée sur une case, peut la lire et y écrire

## Programme 
Liste d'états : I, II, III, ...

Chaque état décrit une action :

Selon ce que contient la case...

- Modifier la case (gomme et crayon)
- Changer d'état ou non ou s'arrêter (HALT)
- Déplacer ou non d'une case à gauche ou à droite

Examples en live sur : \url{https://turingmachine.io/}

# Résultats mathématiques

## Machine _universelle_
Tout ce qui est intuitivement calculable est calculable
avec une machine de Turing. Il existe une machine de Turing
_universelle_ :

- Un programme pour une machine de Turing peut être
  représenté par un nombre.  Il suffit d'utiliser des
  conventions pour décrire états, actions, etc.
- Il existe (quel que soit le codage) une Machine
  de Turing qui prend en entrée sur le ruban:
   - Le codage d'une machine de Turing
   - L'entrée supposée

et qui va l'exécuter (la _simuler_) sur l'entrée

## Indécidabilité de l'_arrêt_
Il n'existe pas de machine de Turing prenant en
entrée une machine de Turing et son entrée et
qui prédit si cette dernière s'arrêtera ou pas.

# La bonne et la mauvaise nouvelle

## La mauvaise
Si on peut fabriquer un ordinateur, on ne pourra jamais
  en faire un autre qui fait quelque chose de plus que les
  autres...

  - On pourra le faire plus vite
  - On pourra le faire moins cher
  - On pourra le faire avec plus de couleurs
  - mais rien de plus (et rien de moins)

# La bonne

## Universalité
En programmation ça s'exprime par la propriété d'être
  _Turing complete_ pour un langage : tous les langages
  de programmation permettent de faire strictement la
  même chose (il suffit de pouvoir y programmer une machine
  de Turing universelle)

  - Pascal, C, Python, JS, ... sont _Turing complete_
  - SQL (de base, standard ISO) ne l'est pas
  - CSS (HTML 5) est Turing complete !

## D'une machine abstraite à une machine concrète

Alan Turing n'a pas seulement inventé une machine théorique.
Il a, pendant la 2nd guerre mondiale, participé à la construction
d'une machine aidant au décryptage des messages ennemis, à
Bletchley Park (UK), dont l'architecture a inspiré les premiers
ordinateurs électroniques.

# Calcul propositionnel

##
On a des _propositions_ élémentaires $p, q, ...$ qui peuvent être
vraies ou fausses et des opérateurs _non_, _ou_, _et_, _implique_,
... notés
$\lnot, \lor, \land, \rightarrow ...$.
Une formule logique est, par exemple :

$$ (p \lor q) \land (\lnot p \rightarrow q) $$

##
Les règles de transformations sont similaires à celle de la théorie
des ensembles (_et_ correspond à _intersection_, _ou_ à _réunion_,
_non_ à _complémentaire_, _implique_ est _inclus dans_).

$$ \lnot (p \lor q) \equiv (\lnot p \land \lnot q) $$

##
Utile à connaître pour simplifier, déboguer un test _if_, _while_, ...

# Table de vérité

##
On peut définir un opérateur par sa _table de vérité_ et 
calculer celle de n'importe quelle expression logique.

##
\begin{displaymath}
\begin{array}{|c c|c|c|c|c|}
p & q & p \land q & p \lor q & p \rightarrow q & p \equiv q\\
\hline 
V & V & V & V & V & V\\
V & F & F & V & F & F\\
F & V & F & V & V & F\\
F & F & F & F & V & V\\
\end{array}
\end{displaymath}

##
\begin{displaymath}
\begin{array}{|c c|c|c|c|c|}
p & q & \lnot p & p \rightarrow q & q \lor \lnot p & (p \rightarrow q) \equiv (q \lor \lnot p) \\ 
\hline 
V & V & F & V & V & V\\
V & F & F & F & F & V\\
F & V & V & V & V & V\\
F & F & V & V & V & V\\
\end{array}
\end{displaymath}

# Déduction naturelle

## Manipulations _syntaxiques_
Règles permettant de _déduire_ des formules à partir d'hypothèses.

##
À gauche : élimination de l'implication (_modus ponens_).

##
\begin{tabular}{cc}
$$\fitchprf{
\pline{A} \\
\pline{A \rightarrow B} }
{ \pline{B} }$$ 
&
$$\fitchprf{
\pline{A \wedge B} }
{ \pline{A} \\
\pline{A \vee C} }$$
\end{tabular}

##
À droite :

- élimination de la conjonction
- introduction de la disjonction.

# Logique des prédicats

## Quantificateurs et prédicats
Introduction de quantificateurs : _il existe_ ($\exists$), _pour tout_
($\forall$) et de _prédicats_ ($P(a)$ _i.e._ P est vrai pour a)
$$ \forall x\; P(x) \iff \lnot \exists x\; \lnot P(x) $$

- Pas de tables de vérité
- Des règles de déduction supplémentaires

##
$$
\fitchprf{
\pline{\forall x Fx}
}
{ \boxedsubproof{c}{}
{ \pline{Fc} \\
\pline{Fc \vee Gc} }
\pline{\forall x (Fx \vee Gx)} }
$$

# Théorèmes de Gödel

## Problème posé par David \textsc{Hilbert} (1862--1943) : 

_Peut-on prouver la consistance de l'arithmétique ? En d'autres termes, peut-on démontrer que les axiomes de l'arithmétique ne sont pas contradictoires ?_

## Kurt \textsc{Gödel} (1906--1978) démontre plusieurs théorèmes :
Un théorème de _complétude_, qui démontre que si une proposition est
_indécidable_ (ni démontrable, ni réfutable) dans une théorie alors il
existe deux modèles de cette théorie, l'un où elle est vraie et l'autre 
où elle est fausse.

## Théorèmes d'incomplétude
- Il existe, dans toute théorie formelle exprimant l'arithmétique,
  des énoncés _indécidables_
- Il est _impossible_ de démontrer la non-contradiction de l'arithmétique

# Raisonner sur un programme

## Vous n'aimez pas les _bugs_ ?
Il n'est pas vain de vouloir prouver qu'un programme est _correct_ !

## Invariants de boucles
Dans toute boucle on devrait pouvoir déterminer une propriété 
_invariante_, toujours _vraie_.

Si elle reste vraie en _sortie_ de boucle on a alors prouvé
que notre code est correct.

## Exemple
Dans l'algorithme de _tri par insertion_, on peut
_démontrer_ que le tableau jusqu'à un certain index est
trié et que la condition de sortie implique que l'index
a atteint la fin du tableau.

D'autres algorithmes de tri auront d'autres invariants
et conditions mais une conclusion finale identique :
le tableau est trié.

# Logique intuitionniste

## Constructionnisme
Issue des réflexions du mathématicien Luitzen Egbertus Jan \textsc{Brouwer}
(1881--1966) sur la notion de _preuve constructive_.

##
Des propositions tautologiques (toujours vraies) en logique classique
ne le sont plus nécessairement :
$$ p \lor \lnot p \mbox{ (tiers exclu)}$$
$$ \lnot \lnot p \rightarrow p $$
$$ \mbox{ (par contre } p \rightarrow \lnot \lnot p \mbox { est vraie}) $$

##
Se généralise à la logique des prédicats.

# Programmes et preuves 

## 
Un théorème de logique intuitionniste impliquant le $\lambda$-calcul
typé.

## Isomorhisme de Curry--Howard
Tout programme est une _preuve_ logique, ce qui est _prouvé_ est
le _type_ du programme.

$$ P(x: A) \rightarrow B \mbox{ est une preuve de } A \Rightarrow B $$

Intuitivement : le programme $P$ transforme une _preuve_ de $A$ en
une _preuve_ de $B$.

## 
C'est la base du développement du système de preuve de programmes
_CoQ_.

Il existe un compilateur C prouvé correct par _CoQ_, ainsi
que des systèmes de commande critiques (aéronautique).

_CoQ_ peut aussi prouver des théorèmes mathématiques.

# Logique·s

## Pour aller plus loin
_Logiques pour l’intelligence artificielle_, P. Gribomon :
\url{https://people.montefiore.uliege.be/lens/logic/notes/LogProp.pdf}

# Algèbre linéaire

##
On rencontre extrêmement souvent des ensembles munis d'une addition
et d'une multiplication par un _« scalaire »_ (typiquement un nombre
réel, plus généralement un élément d'un _corps_ $K$ comme 
$\mathbb{R}$, $\mathbb{C}$ ou un corps fini) tels que :

$$\forall u, v \in E, \forall \lambda \in K : u + \lambda v \in E$$ 

##
Et sur ces ensembles dits _vectoriels_, des fonctions intéressantes (transformations)
sont _linéaires_ :

$$\forall u, v \in E, \forall \lambda \in K :
f(u + \lambda v) = f(u) + \lambda f(v)$$ 

##
Espaces de vecteurs du plan, de l'espace, de fonctions particulières (continues par
exemple), de solutions d'une équation différentielle, de suites,
de polynômes, de séries, ... 

# Bases, dimension, coordonnées

##
Une famille d'éléments (_vecteurs_) est _libre_ ou _linéairement
indépendante_ ssi _aucun_ d'entre eux ne peux être exprimé comme
combinaison linéaire des autres. Autrement dit :
$$ \lambda_1v_1 + \lambda_2v_2 + ... + \lambda_nv_n = 0 \Rightarrow 
\lambda_1 = \lambda_2 = ... = \lambda_n = 0 $$

## Bases
Il existe des familles libres (_bases_) telles que tout vecteur en
soit une combinaison linéaire.

Théorème : toutes les bases ont la même cardinalité, c'est la
_dimension_ (finie ou infinie) de l'espace vectoriel.

## Coordonnées
Les coefficients $\lambda_i$ d'un vecteur $v$ dans une base donnée
sont appelés _coordonnées_ du vecteur $v$.

# Matrices

## 
En dimension finie toute transformation linéaire d'un espace
$E$ vers un espace $F$, de dimensions $n$ et $m$, munies de
bases, peut s'exprimer par un _tableau_ de scalaires avec 
une règle de multiplication spécifique.

## Ce sont des _matrices_.
Leur ensemble forme un _anneau_ $\mathcal{M}_{mn}(K)$.
Il y a des _diviseurs de zéro_.

## En dimension 3 vers 2 

$$
  A\cdot v =
  \begin{pmatrix}
    a_{11} & a_{12} & a_{13} \\
    a_{21} & a_{22} & a_{23} \\
  \end{pmatrix}
  \cdot
  \begin{pmatrix}
    v_{1} \\
    v_{2} \\
    v_{3} \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    a_{11}v_{1} + a_{12}v_{2} + a_{13}v_{3} \\
    a_{21}v_{1} + a_{22}v_{2} + a_{23}v_{3} \\
  \end{pmatrix}
$$

# Matrices et géométrie

##
La multiplication d'une matrice $n\times m$ par une matrice $m\times n$
se fait d'une façon similaire.

## Transformations linéaires
En géométrie une transformation dans le plan où l'espace s'exprime,
étant donnée une base, généralement _orthonormée_, par une matrice
_carrée_ $n\times n$. $2\times 2$ dans le plan, $3\times 3$ dans l'espace.

## Matrices de passage 
Obtenir les coordonnée d'un vecteur dans une base à partir de celle
dans une autre base s'exprime aussi par une matrice ($v = P\cdot u$).
Ces matrices sont _inversibles_ :
$\exists P^{-1}\in \mathcal{M}_n(K) : P\cdot P^{-1} = Id$.

##
Une transformation $T$, ayant comme matrice $A$ dans une base $\mathcal{E}$,
a comme matrice dans une base $\mathcal{E}'$ :
$P^{-1}\cdot A\cdot P$
où $P$ est la matrice _de passage_ entre les deux bases.

# Applications : rotations

## Python ?
Python propose tout ce qu'il faut pour faire de l'algèbre linéaire
en toutes dimensions (_numpy_ et _scipy_), manipuler des transformations
(_pytransform3d_), afficher le résultat (_matplotlib_, _OpenGL_).

## Rotations du plan
_Creating a rotation matrix in NumPy_ :
\url{https://scipython.com/book/chapter-6-numpy/examples/creating-a-rotation-matrix-in-numpy/}

## Dans l'espaaaace
En 3D avec matrices et quaternions :
\url{https://rock-learning.github.io/pytransform3d/rotations.html}

# Complexité algorithmique

##
On peut évaluer en ordre de grandeur le nombre d'opérations
élémentaires (lecture mémoire, écriture, calcul de base) que
réalise un programme comme fonction de la taille de son entrée.

##
Si on attend un nombre en paramètre, la taille est le nombre
de chiffres (ou de bits) du nombre : $\log(n)$.

Si c'est une collection c'est le nombre d'éléments de la collection.

##
La complexité est le comportement asymptotique du temps de calcul,
notation dite « grand $O$ de Landau » :

$$ f(x) \in O(g(x)) \iff \exists k, C\;\; \forall x > k : |f(x)| \leqslant C|g(x)| $$

La constante $k$ (seuil) sert à ignorer les « petites » tailles de
données, quand l'initialisation du programme l'emporte sur les calculs,
et C (facteur) à ne pas dépendre de la vitesse de la machine utilisée. 

# Complexité

## Complexités courantes
- $O(\log(n))$ : logarithmique
- $O(n)$ : linéaire, $O(n^2)$ : quadratique
- $O(n\log(n))$, $O(n\log(\log(n)))$
- Constant : $O(1)$, polynomial : $O(n^p)$
- Exponentiel : $O(\exp(n))$, pire sur-exponentiel $O(\exp(\exp(x)))$

Beaucoup d'algorithmes sont polynomiaux ou en $O(n\log(n))$, la
plupart des problèmes d'optimisation sont exponentiels.

##
Cas d'école : étudiez la complexité des algorithmes de tri.
Examinez les algorithmes choisis par différents langages 
(Python, JS, Java) ou bases de données.

##
On peut aussi calculer la complexité en terme d'espace mémoire
consommé.

# Réduction de complexité

##
Avec un peu de jugeotte on peut diminuer la complexité en
temps ou mémoire d'un calcul.

## Méthode de Ruffini-Horner

$$P(x_{0})=a_{n}x_{0}^{n}+a_{n-1}x_{0}^{n-1}+\ldots +a_{0}$$
Une évaluation directe nécessite $n(n+1)/2$ multiplications et
$n$ additions, complexité $O(n^2)$.
Si on réécrit l'expression ainsi :
$$P(x_{0})=((\dots ((a_{n}x_{0}+a_{n-1})x_{0}+a_{n-2})x_{0}+\dots )x_{0}+a_{1})x_{0}+a_{0}$$
Plus que $n$ multiplications, la complexité est en $O(n)$ !

##
Cerises sur le gâteau : on peut démontrer que c'est le nombre minimal
d'opérations nécessaires et on évite, en plus, d'avoir des valeurs
intermédiaires trop grandes qui pourraient dépasser la capacité du 
type de nombres (entiers, flottants) utilisés.

# P et NP

##
Il existe une classe de problème dit _NP_ où il est « facile »
(polynomial) de vérifier qu'une solution donnée convient mais présumé
« dur » (non-polynomial) d'en trouver une.

Parmi eux, les « plus durs » ont pu êtres prouvé _équivalents_ : on peut
transformer une résolution de l'un d'entre eux en un autre de façon
« facile ». Ce sont les problèmes _NP-complets_.

On ne sait pas si $P = NP$ ou si $P \neq NP$.

##
Autrement dit il se pourrait qu'on puisse trouver une solution
polynomiale pour résoudre tous ces problèmes !

## Un des _problèmes du millénaire_ de l'institut Clay
Un million de dollar à la clef !

Le consensus est que $P \neq NP$, mais on n'en a pas la preuve.

# Complexité algorithmique

## Pour aller plus loin
_Complexité des algorithmes et notation grand O_

Geneviève Savard, Université du Québec :

##
\url{https://cours.etsmtl.ca/SEG/GSavard/mat210/Documents/grandO.pdf}

# Théorie des graphes

##
Un graphe est la donnée d'un ensemble de _nœuds_ reliés par des
_arrêtes_.

Les arrêtes peuvent être orientées ou non, et munies d'un _poid_.

## Structure de données courante
- Liens de transports entre villes
- Réseaux informatiques
- Circuits électriques, électroniques, hydrauliques, ...
- Relations d'amitié dans un groupe
- Systèmes de fichiers hiérarchiques
- Automates, machines de Turing, expressions régulières

## Représentables de plusieurs façons
- Ensembles des nœuds et des arrêtes (couples de nœuds)
- Matrices d'adjacence
- Un des domaines les plus riches de l'_algorithmique_ 

# Représentation graphique 

\begin{tikzpicture}[shorten >=1pt,->]
  \tikzstyle{vertex}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]

  \foreach \name/\x in {s/1, 2/2, 3/3, 4/4, 15/8, 
                        16/9, 17/10, t/11}
    \node[vertex] (G-\name) at (\x,0) {$\name$};

  \foreach \name/\angle/\text in {P-1/234/5, P-2/162/6, 
                                  P-3/90/7, P-4/18/8, P-5/-54/9}
    \node[vertex,xshift=6cm,yshift=4cm] (\name) at (\angle:1cm) {$\text$};

  \foreach \name/\angle/\text in {Q-1/234/10, Q-2/162/11, 
                                  Q-3/90/12, Q-4/18/13, Q-5/-54/14}
    \node[vertex,xshift=6cm,yshift=.5cm] (\name) at (\angle:1cm) {$\text$};

  \foreach \from/\to in {s/2,2/3,3/4,3/4,15/16,16/17,17/t}
    \draw (G-\from) -- (G-\to);

  \foreach \from/\to in {1/2,2/3,3/4,4/5,5/1,1/3,2/4,3/5,4/1,5/2}
    { \draw (P-\from) -- (P-\to); \draw (Q-\from) -- (Q-\to); }

  \draw (G-3) .. controls +(-30:2cm) and +(-150:1cm) .. (Q-1);
  \draw (Q-5) -- (G-15);
\end{tikzpicture}

# Quelques logiciels pour faire des mathématiques

Python (avec _numpy_, _scipy_, _pandas_, _sympy_, _matplotlib_, _jupyter_,
etc.) sait faire beaucoup de choses mais pas tout !

## Impossible, bien sûr, d'être exhaustif
Logiciels _privateurs_ (_propriétaires_) :

- _MATLAB_
- _Mathematica_, _Maple_

## Logiciels libres
- Calcul numérique : _GNU Octave_, _Scilab_, _Genius_
- Calcul formel : _Maxima_, _Axiom_, _SAGE_
- Arithmétique : _Pari GP_
- Géométrie : _Geogebra_ 

## Langages
_R_ (statistiques), 
_Julia_, _Scala_, _CaML_, _Haskell_, _Scheme_, _LISP_, ...

# Pour conclure...

\begin{small}
\emph{La philosophie reste une discipline menacée dans les classes terminales, et les mathématiques un opérateur ennuyeux de sélection sociale. Eh bien moi, je propose la dernière année de maternelle pour les deux : les gamins de cinq ans sauront assurément faire bon usage de la métaphysique de l’infini comme de la théorie des ensembles.}
\begin{flushright}
Alain \textsc{Badiou} avec Georges \textsc{Haéri},
\emph{Éloge des mathématiques},
2015
\end{flushright}
\end{small}

# Références

_The Art of Computer Programming_, Donald Knuth, 1969-2020 Addison-Wesley

_L'informatique à la lumière
de quelques textes de Leibniz_, Laurent Bloch : \url{https://www.epi.asso.fr/revue/articles/a1610b.htm}

_Infinity and the Mind: The Science and Philosophy of the Infinite_, Rudy Rucker, 1982

_Éléments de mathématiques -- Théorie des ensembles_, N. Bourbaki, 1970

_Théorie des ensembles_, Jean-Louis Krivine, 2007

_Gödel, Escher, Bach : Les Brins d'une Guirlande Éternelle_, Douglas Hofstadter, 1979

_On Ducks and Bathtubs_, Ben Bacarisse : \url{http://bsb.me.uk/dd-wealth.pdf}

# Références

Page Oueb du logicien Jean-Yves Girard : \url{https://girard.perso.math.cnrs.fr/Accueil.html}

Page Web du logicien Jean-Louis Krivine : \url{https://www.irif.fr/~krivine/}

_Infinite Reflections_, Peter Suber (configuration SSL du serveur Web invalide, 
mais sans risque !) : \url{https://legacy.earlham.edu/~peters/writing/infinity.htm}

_Diagonalisation de l'hôtel de Hilbert_, Jean-Pierre Messager : \url{https://framagit.org/jpython/math/-/tree/master/Hotel_Hilbert}

_Tout... sur \LaTeX{}_, Vincent Lozano, Framabook 2013 : \url{https://framabook.org/tout-sur-latex/}

_Très courte initiation à \LaTeX{}_, Maxime Chupin : \url{https://www.ceremade.dauphine.fr/~chupin/LaTeX/initLaTeX.pdf} 

# Références

_What Every Computer Scientist Should Know About Floating-Point Arithmetic_,
David Goldberg, Computing Surveys 1991 :
\url{https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html}

Le _Frido_, tout le programme de l'agrégation de mathématiques, librement
disponible :
\url{http://laurent.claessens-donadello.eu/frido.html}

_Solaris_, Stanislas Lem, 1961. En français chez Denoël.

_La voix du maître_, Stanislas Lem, 1976. En français chez Denoël.

_La cité des permutants_, Greg Egan, 1984. En français au Livre de Poche.

Cours _Culture et histoire de l'informatique_, Jean-Pierre Messager :
\url{https://framagit.org/jpython/culthistinfo}

# Références

_Logicomix_, Apóstolos K. Doxiàdis, Christos Papadimitriou, Annie Di Donna
(roman graphique), Vuibert 2018

_Histoire d'algorithmes -- Du caillou à la puce_, Jean-Luc Chabert,
Evelyne Barbin, et _al._, Belin 2010

_Histoire universelle des chiffres_, Georges Ifrah, Robert Laffont 1994

_Alan Turing -- L'homme qui inventa l'informatique_,  David Leavitt,
Dunod 2007

_Science Made Stupid: How to Discomprehend the World Around Us_,
Tom Weller : \url{http://www.chrispennello.com/tweller/}

Web documentaire sur la cryptographie :
\url{https://www.ssi.gouv.fr/particulier/bonnes-pratiques/crypto-le-webdoc/cryptologie-art-ou-science-du-secret/}



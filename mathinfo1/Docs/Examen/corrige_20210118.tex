\documentclass[12pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{geometry}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[french]{babel}
\usepackage[hyphens]{url}
\usepackage{color}
\usepackage{listings}
\usepackage{bbm}

\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

\newcommand\pythonstyle{\lstset{
language=Python,
%basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false            % 
}}

\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}

\usepackage[%
    all,
    defaultlines=3 % nombre minimum de lignes
]{nowidow}


\parskip12pt
%\parindent0pt

\title{Bonnes pratiques de programmation et outils
       mathématiques pour l'informatique}

\author{Jean-Pierre \textsc{Messager}\\[20pt] \textbf{Corrigé}} 

\date{\today} 
% \date{\today} date coulde be today 
% \date{25.12.00} or be a certain date
% \date{ } or there is no date 

\parskip5pt

\begin{document}
% Hint: \title{what ever}, \author{who care} and \date{when ever} could stand 
% before or after the \begin{document} command 
% BUT the \maketitle command MUST come AFTER the \begin{document} command! 
\maketitle

Le code est dans le sous-répertoire \texttt{code} du répertoire où ce 
document est situé dans le dépôt Git.

\section{Limites ensemblistes}

Soit la suite $(E_n)$ de sous-ensembles de $\mathbb{N}$ définie ainsi :

\[
    E_n = \left\{\; n,\; n+1,\; n+2,\; \ldots\; \right\} =
    \left\{ \; k \in \mathbb{N} : k \geqslant n\; \right\}
\]

\begin{itemize}
\setlength\itemsep{1em}
    \item[a.] Peut-on trouver une bijection entre $\mathbb{N}$ et chaque ensemble
   $E_n$ ? En déduire le cardinal $|E_n|$.

    \item[b.] Déterminer la limite supérieure et la limite inférieure de la suite
 $(E_n)$. Admet-elle une limite ensembliste ? Si oui laquelle ?

    \item[c.] Décrire, pour chaque $n \in \mathbb{N}$, la fonction indicatrice
$\mathbbm{1}_{E_n}$ de $\mathbb{N}$ dans $\{ 0, 1 \}$ du sous-ensemble $E_n$
de $\mathbb{N}$. Cette fonction admet-elle une limite (au sens
des fonctions) ? En conclure (en invoquant un théorème cité dans le
cours) la valeur de la limite ensembliste de la suite $E_n$.

    \item[d.] Quel est le cardinal de cet ensemble limite ? 

    \item[e.] En comparant les valeurs de
        $\displaystyle\lim_{n\rightarrow +\infty} |E_n|$ 
        et de $|\lim E_n|$, dans ce cas précis, que peut-on conclure, en général
        pour des suites d'ensembles quelconques concernant les valeurs prises
        par $ \lim \mathbf{card}(E_n)$ et $\mathbf{card}(\lim E_n)$ ?
\end{itemize}

\subsection*{Solution}

\noindent\emph{(a)} Pour tout $n \in \mathbb{N}$ la fonction $f$ définie par 
$f(k) = k + n$ est trivialement une bijection entre $\mathbb{N}$ et $E_n$.

On peut donc en conclure que, pour tout $n$, $|E_n| = |\mathbb{N}| = \aleph_0$.

\noindent\emph{(b)} Pour tout $n \in \mathbb{N}$:
$$\bigcap_{k\geqslant n} E_k = \{n, n+1, ...\} \cap \{n+1, n+2, ...\} \cap \ldots = \varnothing$$

(puisque tout entier $k$ est absent à partir de l'ensemble $E_{k+1}$),
donc :

$$\underline{\lim}\;E_n = \bigcup_{n\geqslant 0}\overbrace{\bigcap_{k\geqslant n} E_k}^{\varnothing} = \bigcup_{n\geqslant 0} \varnothing = \varnothing $$

Pour tout $n \in \mathbb{N}$ :

$$\bigcup_{k\geqslant n} E_k = \{n, n+1, ...\} \cup \{n+1, n+2, ...\} \cup \ldots = \{n, n+1, n+2, ...\} = E_n $$

donc :
$$\overline{\lim}\;E_n = \bigcap_{n\geqslant 0}\overbrace{\bigcup_{k\geqslant n} E_k}^{E_n} = \bigcap_{n\geqslant 0}E_n = \{0, 1, ... \} \cap \{1, 2, ... \} \cap \ldots \cap \{n, n+1, ...\} \cap \ldots = \varnothing $$

(puisque tout entier $n$ est absent à partir de $E_{n+1}$)

Les limites supérieure et inférieure étant égales à $\varnothing$,
la suite $(E_n)$ a une limite qui est $\varnothing$.

Intuitivement : il n'existe aucun nombre naturel $k$ qui serait membre d'un
$E_n$ et aussi de tous les ensembles suivants de la suite : à partir de
l'ensemble $E_{k+1}$, $k$ est absent.

\noindent\emph{(c)} Ceci peut aussi se démontrer en considérant les fonctions indicatrices
$\mathbb{1}_{E_n}$ des ensembles $E_n$ :

$$
\mathbbm{1}_{E_n}(k) = \left\{
    \begin{array}{ll}
        0 & \mbox{si } k < n \\
        1 & \mbox{si } k \geqslant n
    \end{array}
\right.
$$

Donc, pour tout $k \in \mathbb{N}$ :
$$\mathbbm{1}_{E_n}(k) = 0 \mbox{ si } n > k $$

\noindent\emph{i.e.} pour tout $k$, à partir d'un certain indice ($ n \geqslant k+1$)
toutes les fonctions $\mathbbm{1}_{E_n}$ prennent la valeur $0$ en $k$. 

La suite de fonctions $(\mathbbm{1}_{E_n})$ converge donc (au sens
des fonctions) vers la fonction constante : $k\rightarrow 0$, qui est la
fonction indicatrice $\mathbbm{1}_{\varnothing}$ de $\varnothing$.

On en conclut donc encore (c'est heureux !) que la limite de la suite $(E_n)$ est
l'ensemble vide $\varnothing$.

\noindent\emph{(d)} Le cardinal de l'ensemble vide est $0$ : $|\varnothing| = 0$.

\noindent\emph{(e)} Nous avons donc en reprenant, de plus, le résulat établi
à l'étape \emph{a} :

$$
\lim_{n\rightarrow +\infty} |E_n| =  
\lim_{n\rightarrow +\infty} \aleph_0 = \aleph_0  
$$

Nous n'avons, certes, pas défini la notion de limite pour des suites de cardinaux, mais
la suite étant ici \emph{constante} (égale à $\aleph_0$), il est difficile
d'envisager une autre valeur, et certainement pas $0$, cardinal
de l'ensemble vide.

Et aussi :

$$
|\lim E_n| = |\varnothing| = 0
$$

Il est donc \emph{faux}, en général, pour des suites d'ensembles $(U_n)$
(nous avons ici un contre-exemple) que :

$$ \lim |U_n| = |\lim U_n| $$

Ou encore, avec une autre notation, il est \emph{faux}, en général, que :

$$ \lim \mathbf{card}(U_n) = \mathbf{card}(\lim U_n) $$ 

On dit que les \emph{opérateurs}
$\lim$ et $\mathbf{card}$ \emph{ne commutent pas} (en général).

Ce qui s'exprime aussi en disant que la fonction $\mathbf{card}$ n'est
\emph{pas continue}.

Par comparaison, on peut remarquer que dans l'exemple du cours : 
\[
    F_n = \{\; 0, 1,\; \ldots, n\; \} = \{\; k \in \mathbb{N} : k \leqslant n\; \}
\]
\[
    |F_n| = \mathbf{card}(F_n) = n + 1
\]
\[
    \lim F_n = \mathbb{N}
\]
\[
\begin{array}{lllllll}
    \lim |F_n| &=&\lim\mathbf{card}(F_n)  &=& \lim(n+1) &=& +\infty \\
    |\lim F_n| &=&\mathbf{card}(\lim F_n) &=& \mathbf{card}(\mathbb{N}) &=& \aleph_0 \\
\end{array}
\]  

\noindent ce n'est pas
si clair puisque la suite des cardinaux a comme limite $+\infty$ et le
cardinal de la limite est $\aleph_0$. Deux types d'\emph{infinis} en
sorte...

On serait bien tenté de dire qu'ils sont
\emph{égaux}, même si le premier est l'infini de l'analyse (étude des
suites et fonctions numériques) et le second est le premier cardinal infini
au sens de la théorie des ensembles de \textsc{Cantor}.
C'est bien le cas mais dans
un cadre plus rigoureux que nous avons qu'à peine évoqué : l'arithmétique des
nombres \emph{ordinaux}. Ces nombres interviennent dans l'analyse
de complexité des algorithmes.

Le lecteur attentif aura reconnu que la suite d'ensembles ($E_n$)
considérée ici est la même que dans l'article de Ben \textsc{Bacarisse},
\emph{On Ducks and Bathtubs}\footnote{\url{http://bsb.me.uk/dd-wealth.pdf}},
évoqué dans le cours.

\newpage

\section{Des nombres et des mots}

L'objectif est d'écrire un programme en Python 3 capable d'écrire 
un nombre entier naturel en toutes lettres en langue française.

Les règles en usage sont décrites sur de nombreux sites Web, parmi
lesquels :

\noindent\url{https://www.alloprof.qc.ca/fr/eleves/bv/mathematiques/les-regles-d-accord-et-d-ecriture-des-nombres-m1014}

\noindent\url{https://leconjugueur.lefigaro.fr/frlesnombres.php}

\noindent\url{https://bescherelle.ca/ecriture-des-nombres/}

Indiquez quels système de règles vous adoptez (ancienne ou nouvelle
orthographe, français de France, Belgique, Suisse, ...)

Implémentez une fonction ou une classe prenant un entier en argument
qui renvoie
une chaîne de caractères. Par exemple \emph{« dix-huit »} pour $18$
ou \emph{« un-million-deux-cent-quatre-vingt-mille-soixante-et-un »} pour
$1\;280\;061$.

Le script Python doit pouvoir s'utiliser de deux façons :

\begin{itemize}
\setlength\itemsep{1em}
    \item[--] Exécuté en ligne de commande il demande la saisie d'un
        nombre et l'affiche en toute lettres.
    \item[--] Importé dans un autre script (\texttt{import ...} ou
        \texttt{from ... import ...}) il rend possible l'utilisation
        de la fonction ou de la classe.
\end{itemize}

Vous pouvez vérifier le bon fonctionnement de votre programme en 
comparant avec la sortie de ce formulaire en ligne :

\url{https://leconjugueur.lefigaro.fr/nombre/1280061.html}

Utilisez \emph{nose} ou \emph{unittest} pour implémenter la validation
d'un jeu de test pour votre fonction ou classe.

\newpage

\enlargethispage{2cm}
\vspace*{-2.5cm}
\subsection*{Solution}

Le code est disponible dans le dépôt Git, à l'adresse :

\url{https://framagit.org/jpython/math/-/tree/master/mathinfo1/Docs/Examen/code/frenchNumerals}

La convention choisie est la France selon la réforme de 1990.

Le principe général est de diviser, de façon répétée, par 1000 le nombre à traiter
et d'examiner à chaque fois le reste (trois chiffres donc) jusqu'à arriver à
un quotient de zéro.

La chaîne finale est construite de droite à gauche (unités jusqu'à 999, puis milliers, puis millions, etc.)

Le traitement des nombres à trois
\emph{« vrais »} chiffres (\emph{i.e.} non nuls à gauche) est fait par la fonction \texttt{three\_digits\_fr}, qui
délègue si nécessaire à la fonction pour deux chiffres \texttt{two\_digits\_fr}, qui,
à son tour délègue elle aussi le travail (simple) à la fonction
\texttt{single\_digit\_fr}.

Le code est commenté afin de mettre en évidence les étapes du traitement. Les
parties délicates sont l'ajout ou pas de \emph{« -et- »}, la mise au pluriel
ou non de \emph{quatre-vingt}, les nombres de $11$ à $16$, les 70s et les 90s,
l'absence systématique de marque de pluriel pour \emph{mille}.

Le \texttt{README.md} du répertoire décrit l'usage du module et comment
lancer les tests.

Le module fournit une fonction \texttt{number\_to\_French} et peut s'utiliser
en ligne de commande en traitant son argument ou bien interactivement.

Les tests unitaires sont dans le sous-répertoire \texttt{tests}, ils sont
quasiment exhaustifs jusqu'à la limite $10^{24} - 1$, à savoir :

\begin{flushleft}
\emph{neuf--cent--quatre--vingt--dix--neuf--trilliards--neuf--cent--quatre--vingt--dix--neuf--trillions--neuf--cent--quatre--vingt--dix--neuf--billiards--neuf--cent--quatre--vingt--dix--neuf--billions--neuf--cent--quatre--vingt--dix--neuf--milliards--neuf--cent--quatre--vingt--dix--neuf--millions--neuf--cent--quatre--vingt--dix--neuf--mille--neuf--cent--quatre--vingt--dix--neuf}
\end{flushleft}

Des tests exhaustifs prendraient un mois et demi sur la machine utilisée pour le
développement, donc à partir du million des échantillons aléatoires de 100 nombres
sont choisis sur les intervalles million--milliard-\emph{etc.} :
$[ 10^{3k} + 1, 10^{3(k+1)} [$.


Un test est réalisé aussi en lisant un fichier d'échantillons \texttt{test\_sample.txt},
en pratique c'est largement suffisant si le fichier est constitué avec un peu
de soin.

La levée d'exception est testée aussi si le nombre est négatif ou hors-limite.

Le rapport de couverture montre que 100\% du code est couvert syntaxiquement. Je
pense qu'il l'est aussi sémantiquement.

\newpage

Voici une sortie de terminal lors d'un test avec rapport de couverture :

\begin{verbatim}
(venv) .../math/mathinfo1/Docs/Examen/code/frenchNumerals$ make cover
nosetests -v --with-coverage --cover-html tests
Tests for 0-9 with single_digit(), two_digits_fr(), three_digits_fr() ... ok
Tests for 10-20 with two_digits_fr(), three_digits_fr() ... ok
Tests for 21-69 and 80-89 with two_digits_fr(), three_digits_fr() ... ok
Tests for 70s and 90s with two_digits_fr(), three_digits_fr() ... ok
Tests for 100-999 with three_digits_fr() and number_to_French(). ... ok
Tests for 1000-1999 with number_to_French(). ... ok
Tests for hand made sample with usual and tricky cases. ... ok
Tests for 2000-999,999 with number_to_French(). ... ok
Test for exception if out of range. ... ok
Test for exception if negative. ... ok
Tests (sample except for 10**n) for 1,000,000-10**20 - 1 ... ok

Name                Stmts   Miss  Cover
---------------------------------------
frenchNumerals.py      54      0   100%
---------------------------------------
TOTAL                  54      0   100%
----------------------------------------------------------------------
Ran 11 tests in 39.876s

OK
\end{verbatim}

\newpage

\section{Pizzeria}

\emph{Ce problème est volontairement \emph{sous-spécifié}, vous
aurez à faire des choix qui devront avoir du sens, n'hésitez pas à les
justifier dans votre réponse.}

Définissez une classe \emph{Pizza} qui pourrait être utilisée ainsi :

\begin{python}
    giuliaPizza = Pizza(8, 
        ['mozzarella', 'gorgonzola', 'pecorino', 'parmigiano'])
    marcoPizza = Pizza(size=10,
        ingredients = ['pomodoro', 'prosciutto', 'mozzarella'])
    print(giuliaPizza.price() + marcoPizza.price())
    giuliaPizza.discount(20) # rabais de 20%
    marcoPizza.discount(20) # rabais de 20%
    print(giuliaPizza.price() + marcoPizza.price())
\end{python}

Le prix étant calculé comme $\mbox{\emph{taille}}\;^2 / 10 + 3 \times
\mbox{\emph{nombre d'ingrédients}}$.

Une liste d'ingrédients vide lève l'exception \emph{ValueError}.
Une liste qui ne contiendrait pas \emph{uniquement} des chaînes
de caractère lève l'exception \emph{TypeError}.

Voyez-vous l'intérêt de créer des \emph{attributs de classe} dans
la définition de cette classe ? 

Définissez des classes héritant de la classe \emph{Pizza}, 
\emph{Napoletana} et \emph{Margherita}\footnote{et non pas \emph{Marguerita}
comme dans l'énoncé d'origine...}
dont l'usage ne nécessite pas
de passer la liste des ingrédients (cherchez les recettes sur un site
italien ou Wikipedia).

Voyez-vous l'intérêt d'ajouter des \emph{attributs de classe} dans
les définitions de ces classes ? 

Définissez une classe \emph{Order} qui décrit la commande d'une table
et fournit trois méthodes (en plus du constructeur) :

\begin{python}
    table12 = Order(12)
    table12.addPizza(giuliaPizza)
    table12.addPizza(marcoPizza)
    print(table12.price())
    table12.printBill() # Affiche la note
\end{python}

Fournissez un module \emph{pizzeria.py} et un script qui le teste en 
établissant la commande d'une table et affiche la facture.

\newpage

\subsection*{Solution}



Le code est disponible dans le dépôt Git, à l'adresse :

\url{https://framagit.org/jpython/math/-/tree/master/mathinfo1/Docs/Examen/code/pizzeria}

Le module est \texttt{pizzeria.py}, le script qui l'utilise est
\texttt{pizzeria\_go.py}.

La classe \emph{Pizza} est construite selon les spécifications de l'exercice, avec
les spécificités et choix d'implémentation suivants :

\begin{itemize}
    \item[--] Le type \emph{Decimal} est utilisé pour tous les calculs numériques, en
          particulier monétaires
    \item[--] Les tailles permises pour une pizza sont 8, 10 et 12
    \item[--] Ces tailles sont stockées dans un attribut de classe \emph{sizes} de type
          \emph{set}
    \item[--] Si une taille invalide est passée, ou aucune taille, le défaut est 10
    \item[--] Le rabais appliqué est stocké dans un attribut d'instance \emph{rebate}
    \item[--] Un rabais négatif (!!!) ou supérieur à 100\% est interdit (exception
        \emph{ValueError})
    \item[--] Une méthode spéciale \emph{\_\_str\_\_} est fournie pour un affichage
        agréable
\end{itemize}

Un attribut de classe pour les tailles est naturel puisque j'ai choisi de ne
proposer qu'une liste prédéfinie de tailles possibles.

Les classes \emph{Napoletana} et \emph{Margherita} héritent naturellement de la
classe \emph{Pizza}. Un attribut de classe \emph{ingredients}, de type
\emph{list}, définit les ingrédients constitutifs de chaque type de pizza.

Il ne reste alors plus qu'à appeler dans le constructeur de chaque classe le
constructeur de la classe parente avec les arguments idoines.

Comme le mécanisme est le même pour tout type de pizzas, nous pourrions imaginer
de définir une \emph{méta--classe} ou \emph{class factory} qui renvoie une classe
de ce genre pour facilement diversifier notre menu : quatre fromages, 
\emph{primavera}, \emph{etcetera}... !

La classe \emph{Order} permet de prendre une commande à une table donnée et de
fournir une note détaillée à la fin du repas, comme demandé. On ne peut pas commander
autre chose qu'une pizza, sinon une exception \emph{TypeError} est levée.

La facture présente une information utile : la date et l'heure.

\newpage

Voici la sortie du script \texttt{pizzeria\_go.py} :

\begin{verbatim}
Empty collection refused, ok.
('Empty ingredients collection',)
Non-string ingredient refused, ok.
('Non-string ingredient: 42 (int)',)
Pizza(mozzarella, gorgonzola, pecorino, parmigiano| 18.40€)
Pizza(pomodoro, prosciutto, mozzarella| 19.00€)
37.4
110% rebate refused, ok.
('Invalid rebate: 110',)
Pizza(mozzarella, gorgonzola, pecorino, parmigiano| 14.72€)
Pizza(pomodoro, prosciutto, mozzarella| 15.20€)
Pizza(mozzarella, gorgonzola, pecorino, parmigiano| 14.72€)
Pizza(pomodoro, prosciutto, mozzarella| 15.20€)
Napoletana(pomodoro, aglio, origano, olio| 19.80€)
Margherita(pomodoro, mozzarella, basilico, olio| 19.80€)
Total : 69.52
69.52
**** Pizzeria Mario & Luigi ****
17/02/2021 18:12:26
*********** Table 12 ***********
Pizza     	14.72
Pizza     	15.20
Margherita	19.80
Napoletana	19.80
----------
Total     	69.52
\end{verbatim}

\newpage

\noindent\emph{N.B. La partie ci-dessous n'était pas demandée.}

Des tests unitaires sont disponibles dans le sous-répertoire \texttt{tests}.

\emph{Remarque} : en regardant de plus près il semble que \emph{py.test} ait 
beaucoup évolué et que j'ai été un peu rapide en conseillant plutôt
\emph{nose}. \emph{py.test} est supposé être compatible avec \emph{node},
cela gagnerait à être expérimenté.

\begin{verbatim}
(venv) .../math/mathinfo1/Docs/Examen/code/pizzeria$ make cover
nosetests -v --with-coverage --cover-html --cover-package pizzeria tests
test_00pizza.test_pizza_attr ... ok
test_00pizza.test_empty_ingredients ... ok
test_00pizza.test_nonstring_ingredient ... ok
test_00pizza.test_permitted_sizes ... ok
test_00pizza.test_forbidden_size ... ok
test_00pizza.test_default_size ... ok
test_00pizza.test_price_ok ... ok
test_00pizza.test_discount ... ok
test_00pizza.test_invalid_discount_negative ... ok
test_00pizza.test_invalid_discount_too_big ... ok
test_00pizza.test_pointless_str ... ok
test_01childs.test_margherita ... ok
test_01childs.test_napoletana ... ok
test_02order.test_order_attr ... ok
test_02order.test_add_pizza ... ok
test_02order.test_add_not_pizza ... ok
test_02order.test_price ... ok
test_02order.test_total_in_printBill ... ok

Name          Stmts   Miss  Cover
---------------------------------
pizzeria.py      48      0   100%
---------------------------------
TOTAL            48      0   100%
----------------------------------------------------------------------
Ran 18 tests in 0.024s

OK
\end{verbatim}


\end{document}

#!/usr/bin/env python3
'''Return French words for numbers up to 10**24 - 1
   according to 1990 reformed rules.'''

def single_digit_fr(d):
    '''Returns words for a single digit decimal number.'''
    digits_fr = "zéro un deux trois quatre cinq six sept huit neuf".split()
    return digits_fr[d]

def two_digits_fr(dd):
    '''Returns words for up to a double digits decimal number.'''
    if dd < 10:
        return single_digit_fr(dd)
    teens = "dix onze douze treize quatorze quinze seize".split()
    # no "dix-..." or "dix-..."
    if dd < 17:
        return teens[dd - 10]
    elif dd < 20:
        return f"{teens[0]}-{single_digit_fr(dd - 10)}"
    else:
        # Pun on "dozens"
        dizens = "vingt trente quarante cinquante soixante soixante-dix" \
                 " quatre-vingt quatre-vingt-dix".split()
        # dd = d1d0
        d0 = dd  % 10
        d1 = dd // 10

        # Big mess here...
        # "et-un" or not "et-un"? Eighty alone?
        # special cases to consider for "quatre-vingt(s)"
        # 7x and 9x madness
        # This has been designed to be simplified easily for Belgium and
        # Switzerland later

        # No '-et' for 1 unit if 8x (9x is handled later) 
        andifone = '-et' if d0 == 1 and d1 < 8 else ''
        # 7x and 9x ; x from 1 to 6 ; tens go down, units go up
        if (d1 == 7 or d1 == 9) and 0 < d0 < 7:
            start = dizens[d1 - 3]
            end   = '' if d0 == 0 else '-' + two_digits_fr(d0 + 10) 
        # enough of this madness! Usual cases
        else:
            start = dizens[d1 - 2]
            end   = '' if d0 == 0  else '-' + single_digit_fr(d0)
        # Only plural mark for exact 80
        eightymark = 's' if dd == 80 else ''
        return f"{start}{andifone}{end}{eightymark}"

def three_digits_fr(ddd):
    '''Returns words for up to a three digits decimal number.'''
    if ddd < 100:
        return two_digits_fr(ddd)
    hundred = "cent"
    # ddd = d2d10
    d10   = ddd  % 100
    d2    = ddd // 100
    # Simple this time :-)
    pre   = hundred if d2 == 1 else single_digit_fr(d2) + f'-{hundred}'
    end   = '' if d10 == 0 else '-' + two_digits_fr(d10)
    return f"{pre}{end}"

def number_to_French(n):
    '''Returns words for numbers up to 2**24 - 1.'''
    # Can be extended easily
    # Be aware that it exists two distinct scales
    # https://fr.wikipedia.org/wiki/%C3%89chelles_longue_et_courte
    # FR is now usually using the long one, this is not that
    # clear in English (UK vs. USA)...

    # Get rid of zero once and for all 
    if n == 0:
        answer = three_digits_fr(n)
        return answer

    third_powers_of_ten = "mille million milliard billion billiard" \
                          " trillion trilliard".split()
    # Warming up...
    r = n  % 1000
    n = n // 1000
    # Three zeroes at the end? Starts from '' then.
    answer = '' if r == 0 else three_digits_fr(r)
    p = 0
    while n != 0:
        r = n  % 1000
        n = n // 1000
        # Nothing at this order 
        if r == 0:
            p += 1
            continue
        # Push how much at this order
        # No "un" if 1000 (mille)
        pre    = '' if p == 0 and r == 1 else three_digits_fr(r) + '-' 
        order  = third_powers_of_ten[p]
        # Do not mark plural for "mille", mark for other orders
        plural = 's' if r > 1 and p > 0 else ''
        # No dash at the end of nothing...
        sep    = '-' if answer else ''
        answer = f"{pre}{order}{plural}{sep}{answer}"
        p += 1
    return answer 

if __name__ == '__main__': # pragma: no cover
    import sys
    from math import *
    if len(sys.argv) > 1:
        try:
            n = int(eval(sys.argv[1]))
        except Exception as e:
            print(f"{e.__class__.__name__}: {e.args[0]}")
            exit(1) 
        print(f"{n:,}\n{number_to_French(n)}")
        exit(0)

    while True:
        ok = False
        while not ok:
            n = input('Decimal number (END to quit): ')
            if n.strip() == 'END':
                exit(0)
            try:
                n = int(eval(n))
            except Exception as e:
                print(f"{e.__class__.__name__}: {e.args[0]}")
                continue
            if n >= 10**24 or n < 0:
                print(f"{n}: too big or negative.")
                continue
            ok = True
        print(f"{n:,}\n{number_to_French(n)}")



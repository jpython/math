#!/usr/bin/env python3

import requests
import sys

if len(sys.argv) < 2:
    sys.stderr.write(f"Usage: {sys.arg[0]} n")
    exit(1)

n = int(sys.argv[1])

url = f'https://leconjugueur.lefigaro.fr/nombre/{n}.html' 

with requests.get(url) as r:
    if r.ok:
        r.content


zéro    0
un      1
deux    2
trois   3
quatre  4
cinq    5
six     6
sept    7
huit    8
neuf    9
dix     10
onze    11
douze   12
treize  13
quatorze    14
quinze  15
seize   16
dix-sept    17
dix-huit    18
dix-neuf    19
vingt       20

vingt-et-un 21
vingt-cinq  25

trente      30
cinquante   50
soixante    60
soixante-et-un  61
soixante-trois  63

soixante-dix    70
soixante-douze  72
quatre-vingts       80
quatre-vingt-un     81
quatre-vingt-sept   87

quatre-vingt-dix    90
quatre-vingt-onze   91
quatre-vingt-dix-huit   98

cent    100
cent-un 101
cent-douze  112

deux-cent   200

trois-cent-vingt-quatre 324

mille   1000
deux-mille  2000
quatre-mille-un 4001

un-million  1000000
deux-millions 2000000
sept-millions-douze 7000012

un-milliard 1000000000
trois-milliards-mille-un 3000001001

neuf-cent-quatre-vingt-dix-neuf-billions-neuf-cent-quatre-vingt-dix-neuf-milliards-neuf-cent-quatre-vingt-dix-neuf-millions-neuf-cent-quatre-vingt-dix-neuf-mille-neuf-cent-quatre-vingt-dix-neuf 999999999999999

neuf-cent-quatre-vingt-dix-neuf-billions-neuf-cent-quatre-vingt-dix-neuf-milliards-neuf-cent-quatre-vingt-onze-mille-deux-cent-trente-neuf 999999000991239

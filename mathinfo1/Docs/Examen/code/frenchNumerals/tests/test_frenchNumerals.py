#!/usr/bin/env python3

# Règle : un test doit être moins complexe que le
# code qu'il teste... (sinon il sera plus compliqué
# à... tester)

from nose.tools import raises
from frenchNumerals import (single_digit_fr, 
                            two_digits_fr,
                            three_digits_fr,
                            number_to_French)


def valid_collections(numbers, words, func):
    '''Test a iterable of numbers against a iterable of
    words, testing if func(n) == w.'''
    for n,w in zip(numbers, words):
        assert func(n) == w 

def test_single_d():
    '''Tests for 0-9 with single_digit(), two_digits_fr(), three_digits_fr()
    and number_to_French()'''
    words   = "zéro un deux trois quatre cinq six sept huit neuf".split()
    numbers = range(0,10)
    valid_collections(numbers, words, single_digit_fr)
    valid_collections(numbers, words, two_digits_fr)
    valid_collections(numbers, words, three_digits_fr)
    valid_collections(numbers, words, number_to_French)

def test_teens():
    '''Tests for 10-20 with two_digits_fr(), three_digits_fr()
    and number_to_French().'''
    words   = "dix onze douze treize quatorze quinze seize" \
              " dix-sept dix-huit dix-neuf vingt".split()
    numbers = range(10,21)
    valid_collections(numbers, words, two_digits_fr)
    valid_collections(numbers, words, three_digits_fr)
    valid_collections(numbers, words, number_to_French)

def test_easy_two_digits():
    '''Tests for 21-69 and 80-89 with two_digits_fr(), three_digits_fr()
    and number_to_French().'''
    from itertools import chain
    tens    = 'vingt trente quarante cinquante soixante quatre-vingt'.split()
    units   = "_ un deux trois quatre cinq six sept huit neuf".split()
    tens_n  = chain(range(2,7), (8,))
    units_n = range(0,10)
    from itertools import product
    for (start,end),(t,u) in zip(product(tens, units), \
                                 product(tens_n, units_n)):
        if u == 0:
            # quatre-vingts
            mark = 's' if t == 8 else ''
            assert two_digits_fr(10*t) ==  f"{start}{mark}"
            assert three_digits_fr(10*t) ==  f"{start}{mark}"
            assert number_to_French(10*t) ==  f"{start}{mark}"
        else:
            # quatre-vingt-un not quatre-vingt-et-un
            middle = '-et-' if u == 1 and t != 8 else '-'
            assert two_digits_fr(10*t + u) ==  f"{start}{middle}{end}"
            assert three_digits_fr(10*t + u) ==  f"{start}{middle}{end}"
            assert number_to_French(10*t + u) ==  f"{start}{middle}{end}"

def test_7x9x():
    '''Tests for 70s and 90s with two_digits_fr(), three_digits_fr()
    and number_to_French().'''
    from itertools import chain

    numbers = chain(range(70,80), range(90,100))

    words7x = "soixante-dix soixante-et-onze soixante-douze soixante-treize" \
              " soixante-quatorze soixante-quinze soixante-seize"            \
              " soixante-dix-sept soixante-dix-huit soixante-dix-neuf".split()

    words9x = "quatre-vingt-dix quatre-vingt-onze quatre-vingt-douze"           \
              " quatre-vingt-treize quatre-vingt-quatorze quatre-vingt-quinze"   \
              " quatre-vingt-seize quatre-vingt-dix-sept quatre-vingt-dix-huit" \
              " quatre-vingt-dix-neuf".split()

    valid_collections(numbers, chain(words7x, words9x), two_digits_fr)
    valid_collections(numbers, chain(words7x, words9x), three_digits_fr)
    valid_collections(numbers, chain(words7x, words9x), number_to_French)

# Note: adding this test didn't raise coverage percentage, coverage measure cannot
# be perfect
def test_hundreds():
    '''Tests for 100-999 with three_digits_fr() and number_to_French().'''
    hundred = '''cent'''
    units   = "deux trois quatre cinq six sept huit neuf".split()
    assert three_digits_fr(100)  == hundred 
    assert number_to_French(100) == hundred
    for n in range(101, 200):
        assert three_digits_fr(n) == f"{hundred}-{number_to_French(n - 100)}" 
        assert number_to_French(n) == f"{hundred}-{number_to_French(n - 100)}" 
    for n,u in zip(range(2, 10), units):
        assert three_digits_fr(n*100) == f"{u}-{hundred}"
        assert number_to_French(n*100) == f"{u}-{hundred}"
        for i in range(1, 100):
            assert three_digits_fr(n*100 + i) == f"{u}-{hundred}-{number_to_French(i)}" 
            assert number_to_French(n*100 + i) == f"{u}-{hundred}-{number_to_French(i)}" 

# up to 96%!
def test_one_thousand():
    '''Tests for 1000-1999 with number_to_French().'''
    thousand = 'mille'
    assert number_to_French(1000) == thousand
    for i in range(1, 1000):
        assert number_to_French(1000 + i) == f"{thousand}-{number_to_French(i)}"

# This should have be enough, but I wanted to take fun!
def test_sample():
    '''Tests for hand made sample with usual and tricky cases.'''
    with open('tests/test_sample.txt') as s:
        for line in s:
            if not (line := line.strip()):
                continue
            word, n = line.split()
            n = int(n)
            assert number_to_French(n) == word


# no change in coverage percentage... even if biggest test so far!
def test_thousands():
    '''Tests for 2000-999,999 with number_to_French().'''
    thousand = 'mille'
    for n in range(2,999):
        assert number_to_French(n*1000) == f"{number_to_French(n)}-{thousand}"
        for i in range(1, 100):
            assert number_to_French(n*1000 + i) == \
                f"{number_to_French(n)}-{thousand}-{number_to_French(i)}"

@raises(IndexError)
def test_out_of_range():
    '''Test for exception if out of range.'''
    n = number_to_French(10**24)

@raises(IndexError)
def test_negative():
    '''Test for exception if negative.'''
    n = number_to_French(-1)

# 100% coverage even if semantically not yet.
# on my system (Intel Core i5) full test should take about
# a month or so...
def test_illions():
    '''Tests (sample except for 10**n) for 1,000,000-10**20 - 1 
    with number_to_French().'''
    from random import sample
    illions = "million milliard billion billiard trillion trilliard".split()
    sample_size = 100 # for 10**(3n) + k, ~ 30s on iCore 5 
    for p,illion in zip(range(6,20,3), illions):
        for i in range(1, 999):
            mark = 's' if i > 1 else ''
            assert number_to_French(i*10**p) == \
                f"{number_to_French(i)}-{illion}{mark}"
            # full test, not practical
            #for j in range(1, 10**6):
            #    assert number_to_French(i*10**p + j) == \
            #        f"{number_to_French(i)}-{ion}{mark}-{number_to_French(j)}"
            for j in sample(range(1, 10**6), sample_size):
                assert number_to_French(i*10**p + j) == \
                    f"{number_to_French(i)}-{illion}{mark}-{number_to_French(j)}"



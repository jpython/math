# From Whole Numbers to French Numerals

## Introduction

This module provide a function `number_to_French` accepting a non-negative
number as an argument and returning a string.

Chosen rules are from 1990 reform in France (things are a little easier
in Belgium or Switzerland).

Installation and setting up virtual environment:

~~~~
$ make venv # on Microsoft Windows, read the Makefile for additional steps...
$ source venv/bin/activate 
(or C:\...> venv\Scripts\activate.bat)
~~~~

## Usage

As a script:

~~~~
$ ./frenchNumerals.py 123456
123,456
cent-vingt-trois-mille-quatre-cent-cinquante-six
$
$ ./frenchNumerals.py
Decimal number (END to quit): 123
123
cent-vingt-trois
Decimal number (END to quit): 89000101
89,000,101
quatre-vingt-neuf-millions-cent-un
Decimal number (END to quit): 10**6 + 78*10**3 + 42
1,078,042
un-million-soixante-dix-huit-mille-quarante-deux
Decimal number (END to quit): END
~~~~

As a module:

~~~~Python
from frenchNumerals import number_to_French

print(number_to_French(42))
~~~~

## Tests

The `tests` subdirectory provides unit testing :

~~~~
$ make cover # or make test to skip cover reports
nosetests -v --with-coverage --cover-html tests
Tests for 0-9 with single_digit(), two_digits_fr(), three_digits_fr() ... ok
Tests for 10-20 with two_digits_fr(), three_digits_fr() ... ok
Tests for 21-69 and 80-89 with two_digits_fr(), three_digits_fr() ... ok
Tests for 70x and 90s with two_digits_fr(), three_digits_fr() ... ok
Tests for 100-999 with three_digits_fr() and number_to_French(). ... ok
Tests for 1000-1999 with number_to_French(). ... ok
Tests for hand made sample with usual and tricky cases. ... ok
Tests for 2000-999,999 with number_to_French(). ... ok
Test for exception if out of range. ... ok
Test for exception if negative. ... ok
Tests (sample except for 10**n) for 1,000,000-10**20 - 1 ... ok

Name                Stmts   Miss  Cover
---------------------------------------
frenchNumerals.py      54      0   100%
---------------------------------------
TOTAL                  54      0   100%
----------------------------------------------------------------------
Ran 11 tests in 32.870s

OK
~~~~

## TODO

- Add a flag to switch to pre-1990 rules, Belgium, Switzerland
- Build more tests vs. _conjugueur_ Web Site
- Write the reverse function (the other way around: from words to number)
- Support other languages 

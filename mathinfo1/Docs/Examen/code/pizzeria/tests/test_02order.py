#!/usr/bin/env python3

from nose.tools import raises
from decimal import Decimal
import contextlib
from pizzeria import Order, Pizza

def test_order_attr():
    o = Order(42)
    assert o.tablenum == 42
    assert o.pizze == []

def test_add_pizza():
    o = Order(42)
    p = Pizza(8, [ 'ananas', 'bacon', 'spam' ])
    o.addPizza(p)
    assert o.pizze == [p]

@raises(TypeError)
def test_add_not_pizza():
    o = Order(42)
    o.addPizza('spam')

def test_price():
    o = Order(42)
    p1 = Pizza(8, [ 'ananas', 'bacon', 'spam' ])
    p2 = Pizza(10, [ 'egg', 'ananas', 'bacon', 'spam' ])
    o.addPizza(p1)
    o.addPizza(p2)
    t = p1.price() + p2.price()
    assert o.price() == t

def test_total_in_printBill():
    from io import StringIO
    import re, sys
    o = Order(42)
    p1 = Pizza(8, [ 'ananas', 'bacon', 'spam' ])
    p2 = Pizza(10, [ 'egg', 'ananas', 'bacon', 'spam' ])
    o.addPizza(p1)
    o.addPizza(p2)
    t = o.price()
    temp_stdout = StringIO()
    with contextlib.redirect_stdout(temp_stdout):
        o.printBill()
        output = temp_stdout.getvalue().strip()
        assert re.search(rf'Total(\s+){t:.2f}\Z', output) is not None

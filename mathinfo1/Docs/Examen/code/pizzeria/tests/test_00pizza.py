#!/usr/bin/env python3

from nose.tools import raises
from decimal import Decimal
from pizzeria import Pizza

def test_pizza_attr():
    p = Pizza(8, [ 'ananas', 'bacon', 'spam' ])
    assert p.size == 8
    assert p.ingredients == [ 'ananas', 'bacon', 'spam' ]
    assert p.rebate == 0

@raises(ValueError)
def test_empty_ingredients():
    p = Pizza(8, [ ])

@raises(TypeError)
def test_nonstring_ingredient():
    p = Pizza(12, [ 'egg', 'ananas', 42 ])

def test_permitted_sizes():
    p = Pizza(8, [ 'ananas', 'bacon', 'spam' ])
    assert p.size == 8
    p = Pizza(10, [ 'ananas', 'bacon', 'spam' ])
    assert p.size == 10 
    p = Pizza(12, [ 'ananas', 'bacon', 'spam' ])
    assert p.size == 12

def test_forbidden_size():
    p = Pizza(42, [ 'ananas', 'bacon', 'spam' ])
    assert p.size == 10

def test_default_size():
    p = Pizza(ingredients=[ 'ananas', 'bacon', 'spam' ])
    assert p.size == 10

def test_price():
    size = 12
    ingredients = [ 'ananas', 'bacon', 'spam' ] 
    p = Pizza(size, ingredients)
    assert p.price() == size**2/Decimal(10) + 3*len(ingredients)

def test_discount():
    rebate = 35
    p = Pizza(42, [ 'ananas', 'bacon', 'spam' ])
    oldprice = p.price()
    p.discount(35)
    assert p.price() == oldprice * (100 - rebate)/100

@raises(ValueError)
def test_invalid_discount_negative():
    p = Pizza(42, [ 'ananas', 'bacon', 'spam' ])
    p.discount(-10)

@raises(ValueError)
def test_invalid_discount_too_big():
    p = Pizza(42, [ 'ananas', 'bacon', 'spam' ])
    p.discount(110)

def test_pointless_str():
    p = Pizza(42, [ 'ananas', 'bacon', 'spam' ])
    assert str(p) != ''



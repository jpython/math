#!/usr/bin/env python3

from pizzeria import Margherita, Napoletana

def test_margherita():
    p = Margherita(10)
    assert p.ingredients == 'pomodoro,mozzarella,basilico,olio'.split(',')

def test_napoletana():
    p = Napoletana(12)
    assert p.ingredients == 'pomodoro,aglio,origano,olio'.split(',')

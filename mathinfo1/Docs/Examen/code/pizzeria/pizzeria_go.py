#!/usr/bin/env python3

from pizzeria import Pizza, Napoletana, Margherita, Order

try:
    fault = Pizza(8, [])
except ValueError as e:
    print("Empty collection refused, ok.")
    print(e.args)
   
try:
    fault = Pizza(8, ['mozzarella', 42, 'pecorino', 'parmigiano'])
except TypeError as e:
    print("Non-string ingredient refused, ok.")
    print(e.args)

giuliaPizza = Pizza(8, ['mozzarella', 'gorgonzola', 'pecorino', 'parmigiano'])
marcoPizza  = Pizza(size=10,
                    ingredients=['pomodoro', 'prosciutto', 'mozzarella'])

dinoPizza = Napoletana(10)
elviraPizza = Margherita()

print(giuliaPizza, marcoPizza, sep='\n')
print(giuliaPizza.price() + marcoPizza.price())

try:
    giuliaPizza.discount(110)
except ValueError as e:
    print('110% rebate refused, ok.')
    print(e.args)

giuliaPizza.discount(20)
marcoPizza.discount(20)
print(giuliaPizza, marcoPizza, sep='\n')

dinoPizza.discount(10)
elviraPizza.discount(10)

tutteLePizze = [ giuliaPizza, marcoPizza, dinoPizza, elviraPizza ]

print(*tutteLePizze, sep='\n')
print(f"Total : {sum(pizza.price() for pizza in tutteLePizze):.2f}")

table12 = Order(12)
table12.addPizza(giuliaPizza)
table12.addPizza(marcoPizza)
table12.addPizza(elviraPizza)
table12.addPizza(dinoPizza)
print(table12.price())
table12.printBill() # Affiche la note

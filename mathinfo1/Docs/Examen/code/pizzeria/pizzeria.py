#!/usr/bin/env python3

from datetime import datetime
from decimal import Decimal

class Pizza:
    '''Generic Pizza'''
    sizes = { Decimal(8), Decimal(10), Decimal(12) }
    def __init__(self, size=10, ingredients=[]):
        if len(ingredients) == 0:
            raise ValueError("Empty ingredients collection")
        if not all( isinstance(_ := item, str) for item in ingredients ):
            raise TypeError(f"Non-string ingredient: {_!r} ({type(_).__name__})")
        self.size = size if size in Pizza.sizes else Decimal(10)
        self.ingredients = ingredients
        self.rebate = 0
    def price(self):
        return (self.size**2/Decimal(10) + \
                3*len(self.ingredients))*(100 - self.rebate)/100
    def discount(self, rebate):
        if 0 <= rebate <= 100:
            self.rebate = Decimal(rebate)
        else:
            raise ValueError(f"Invalid rebate: {rebate!r}")
    def __str__(self):
        return f"{type(self).__name__}" \
               f"({', '.join(str(n) for n in self.ingredients)}" \
               f"| {self.price():.2f}€)"

class Margherita(Pizza):
    '''Pizza Margherita'''
    ingredients = 'pomodoro,mozzarella,basilico,olio'.split(',')
    def __init__(self, size=10):
        super().__init__(size, Margherita.ingredients)

class Napoletana(Pizza):
    '''Pizza Napoletana'''
    ingredients = 'pomodoro,aglio,origano,olio'.split(',')
    def __init__(self, size=10):
        super().__init__(size, Napoletana.ingredients)

class Order:
    '''Tanti pizze per nostri ospiti, pero e mandatorio di pagare...'''
    def __init__(self, tablenum):
        self.tablenum = tablenum
        self.pizze = []
    def addPizza(self, pizza):
        if isinstance(pizza, Pizza):
            self.pizze.append(pizza)
        else:
            raise TypeError(f"Not a Pizza : {pizza!r} ({type(pizza).__name__})")
    def price(self):
        return sum( pizza.price() for pizza in self.pizze )
    def printBill(self):
        print('**** Pizzeria Mario & Luigi ****')
        print(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        print('*' * 11, f"Table {self.tablenum:2}", '*' * 11)
        total = 0
        for p in self.pizze:
            total += (price := p.price())
            print(f"{type(p).__name__:10}\t{price:.2f}")
        print('-' * 10)
        print(f"{'Total':10}\t{total:.2f}")


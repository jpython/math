#!/usr/bin/env python3

def S(x):
    return x.S()

class Peano(str):
    def __new__(cls, ch='0', debug=False):
        self = super().__new__(cls, ch)
        self.debug = debug
        return self
    @staticmethod
    def log(*args, **kwargs):
        print(*args, **kwargs)
    def S(self):
        return Peano('S' + self)
    def __int__(self):
        return self.count('S')

if __name__ == '__main__':
    zero = Peano(debug=False)
    un   = zero.S()
    deux = zero.S().S()
    trois = deux.S()
    quatre = S(trois)
    print('  Zero', zero)
    print('    Un', un)
    print('  Deux', deux)
    print(' Trois', trois)
    print('Quatre', quatre)


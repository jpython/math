\documentclass[10pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[a4paper,left=2cm,right=1.5cm,top=2cm,bottom=2cm]{geometry}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage[french]{babel}
\usepackage{multicol}
\usepackage{color}
\usepackage{listings}
\usepackage{bbm}
\usepackage{graphicx}
\usepackage{wrapfig}

%\usepackage[%
%    all,
%    defaultlines=3 % nombre minimum de lignes
%]{nowidow}

\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{9} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{9}  % for normal

\newcommand\pythonstyle{\lstset{
language=Python,
%basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false            %
}}

\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

\addto\captionsfrench{\def\figurename{}}

\parskip12pt
%\parindent0pt

\title{Représentation d'un schéma d'axiomes\\ sous forme de classes 
       abstraites\\ --- le cas de l'axiomatique de Peano ---}
\author{Jean-Pierre \textsc{Messager}}

\date{\today\thanks{pour Élaine.}}
% \date{\today} date coulde be today 
% \date{25.12.00} or be a certain date
% \date{ } or there is no date 
\begin{document}
% Hint: \title{what ever}, \author{who care} and \date{when ever} could stand 
% before or after the \begin{document} command 
% BUT the \maketitle command MUST come AFTER the \begin{document} command! 
\maketitle

\begin{abstract}
L'axiomatique de Peano fait partie de ce qu'on appelle un \emph{schéma
d'axiomes} (il est constitué d'une infinité dénombrables d'axiomes).
L'axiomatisation de l'arithmétique par Peano
se contente de supposer l'existence d'un objet (l'ensemble $\mathbb{N}$)
dont les propriétés sont ensuite énoncées.

Les \emph{concrétisations} de ces axiomes sont d'ailleurs bien
connues, sous formes de \emph{modèles}. Il y en a plusieurs :
les entiers de \textsc{Von Neumann} en sont le plus célèbre.

La relation entre schéma d'axiomes et modèles est similaire à
cette entre \emph{classes abstraites} et \emph{concrète} en
programmation orienté objet. Nous illustrons cette correspondance
	dans le cas de l'arithmétique de Peano en \mbox{Python}.
\end{abstract}

\begin{multicols}{2}
\section{Introduction}

\begin{wrapfigure}{r}{40mm}
  \centering
  \includegraphics[width=0.9\linewidth]{Giuseppe_Peano.jpg}
  \caption{Giuseppe \textsc{Peano}}
\end{wrapfigure}

\vspace{-0.5cm}
Giuseppe \textsc{Peano} (1858-1932) a proposé en 1889 une axiomatisation
de l'arithmétique basé sur la notion de \emph{successeur}
dans le langage de la logique du premier ordre. L'idée
	générale est proche de ce que Richard \mbox{\textsc{Dedekind}} avait
proposé auparavant d'une façon moins formelle.


Cette axiomatisation a donné lieu à de nombreuses implémentations
dans les langages de programmation ou des systèmes de preuves.

Nous allons exposer comment l'articulation des deux jeux
d'axiomes de Peano peut s'illustrer par la distinction
entre classes abstraites et concrètes en langage Python
(dans sa version 3).

Nous montrerons comment implémenter
plusieurs modèles de ce schéma d'axiomes sous forme de classes
concrètes tout en héritant d'une classe abstraite qui fournit
les opérations arithmétiques.

Il existe de nombreuses présentations de l'axiomatique de Peano,
toutes équivalentes bien entendu. La partie définissant $\mathbb{N}$,
ensemble des entiers naturels, peut s'écrire ainsi :

%\columnbreak
\begin{itemize}
	\item[-] $0 \in \mathbb{N}$
	\item[-] Pour tout $n \in \mathbb{N}$, il existe $Sn \in \mathbb{N}$
  (successeur de $n$)
	\item[-] Il n'y a aucun élément $n$ de $\mathbb{N}$ tel que $Sn = 0$
	\item[-] Si $Sn = Sm$ alors $n = m$ (S est \emph{injective})
	\item[-] Si $E \subset \mathbb{N}$, $0\in E$ et ($n\in E \Rightarrow Sn\in E$)
	alors $E = \mathbb{N}$
\end{itemize}

Les opération d'addition et de multiplication se définissent alors ainsi :

\noindent Pour l'addition :
\begin{itemize}
	\item[-] \emph{(a0)} $n + 0 = n$
	\item[-] \emph{(a1)} $x + Sy = S(x + y)$
\end{itemize}

~\\ Pour la multiplication :
\begin{itemize}
	\item[-] \emph{(m0)} $n\times 0 = 0$
	\item[-] \emph{(m1)} $x\times Sy = (x\times y) + x$
\end{itemize}

~\\
Nous allons voir comment le premier jeu d'axiomes peut se représenter
comme une classe concrète (donc représentant un \emph{modèle}
particulier de l'arithmétique de Peano, tandis que le second
jeu, lui, sera représenté par une classe abstraite dont héritera
chaque modèle. 

\section{Une première représentation sous forme de classe}

	Une première représention, héritant du type intégré \emph{str}
	de Python (chaînes de caractères) et refletant le premier jeu
	d'axiomes sous une forme purement syntaxique est :

\begin{python}
def S(x):
    return x.S()

class Peano(str):
    def __new__(cls, ch='0'):
        self = super().__new__(cls, ch)
        return self 
    def S(self):
        return Peano('S' + self)
    def __int__(self):
        return self.count('S')
\end{python}

Le code ci-dessus peut être testé ainsi :

\begin{python}
if __name__ == '__main__':
    zero   = Peano(debug=False)
    un     = zero.S()
    deux   = zero.S().S()
    trois  = deux.S()
    quatre = S(trois)
    print('  Zero', zero)
    print('    Un', un)
    print('  Deux', deux)
    print(' Trois', trois)
    print('Quatre', quatre)
\end{python}	

L'exécution dans un terminal produit ceci :

\begin{verbatim}
  Zero 0
    Un S0
  Deux SS0
 Trois SSS0
Quatre SSSS0
\end{verbatim}

\section{Isolation de la partie axiomatique comme classe abstraite}

ouais ouais

\section{Un premier modèle, syntaxique}

il fait chaud

\section{Le modèle de \textsc{Von Neumann}}

et beau...

\section{Le modèle de jesaisplusqui}

faut retrouver le nom.

\section{$\lambda$-calcul et entiers de \textsc{Church}}

ah ouais ?

\section{Modèles non-standards}

pas dit qu'on y arrive à ça... Haskell ???

\end{multicols}

\end{document}

